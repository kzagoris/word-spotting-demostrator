import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'rtruncate'
})
export class ReverseTruncatePipe implements PipeTransform {

  transform(value: string, args?: number): string {
    const limit = args;
    return value.length > limit ? '...' + value.substring(value.length - limit - 1, value.length) : value;
  }

}
