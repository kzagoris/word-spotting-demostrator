import {Component, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import {IDocument} from '../../models/idocument';
import {ISearchResult} from '../../models/isearch-result';
import {StorageService} from '../../providers/storage.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-browse-result',
  templateUrl: './browse-result.component.html',
  styleUrls: ['./browse-result.component.scss']
})
export class BrowseResultComponent implements OnInit {

  @ViewChild('mainDocument') mainDocumentRef: ElementRef;
  document$: BehaviorSubject<IDocument | null>;
  document: IDocument;
  word: { x: number, y: number, width: number, height: number } = {x: 0, y: 0, width: 0, height: 0};
  @Input() result: ISearchResult;
  @Input() collectionId: number;

  constructor(private storage: StorageService) {
    this.document$ = new BehaviorSubject<IDocument | null>(null);
  }

  ngOnInit(): void {
    if (!this.collectionId || !this.result) {
      return;
    }
    this.storage.getDBCollection((this.collectionId.toString())).then(s => {
      if (s.documents) {
        this.document = s.documents.find(c => c.id === Number(this.result.imageId));
        this.document$.next(this.document);
      }
    });

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.calculateWordDims();
  }
  onMainDocumentLoad() {
    this.calculateWordDims();
  }

  calculateWordDims() {
    const document = this.mainDocumentRef.nativeElement;
    const aspectRatio = document.width / document.naturalWidth;
    this.word = {
      x: aspectRatio * this.result.location[0],
      y: aspectRatio * this.result.location[1],
      width: aspectRatio * this.result.location[2],
      height: aspectRatio * this.result.location[3],
    }
  }



}
