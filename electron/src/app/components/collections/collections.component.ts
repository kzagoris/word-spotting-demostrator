import { Component, OnInit } from '@angular/core';
import { WsService } from '../../providers/ws.service';
import { Observable } from 'rxjs/Observable';
import { ICollectionListing } from '../../models/icollection-listing';
import { StorageService } from '../../providers/storage.service';
import { MatDialog } from "@angular/material";
import { DialogConfirmationComponent } from "../dialog-confirmation/dialog-confirmation.component";


@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.scss']
})
export class CollectionsComponent implements OnInit {

  public collectionList$: Observable<ICollectionListing[]>;

  constructor(public wsService: WsService, public storage: StorageService, public dialog: MatDialog) {

  }

  async deleteCollection(id: number) {
    let dialogRef = this.dialog.open(DialogConfirmationComponent, {
      width: '250px',
      data: { title: 'Delete Collection', message: 'Are you sure?' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.wsService.deleteCollection(id).subscribe(async d1=> {
          await this.storage.deleteDBCollection(id.toString());
          this.collectionList$ = this.wsService.getCollectionListing(true);
        });
      }
    });

  }

  ngOnInit() {
    this.collectionList$ = this.wsService.getCollectionListing(true);
    this.wsService.getActiveCollectionListing().subscribe(listings => {
      if (listings != null) {
        for (const l of listings) {
          this.wsService.deactivateCollection(l.id).subscribe();
        }
      }
    });
  }

}
