import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';

export class SearchResolve implements Resolve<{ title: string, link: string }> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
  : Observable<{ title: string; link: string }> | Promise<{ title: string; link: string }> | { title: string; link: string } {
    const collectionId = route.params['collectionId'];
    const documentId = route.params['documentId'];
    return {
      title: 'go back', link: `document/${collectionId}/${documentId}`
    }
  }
}
