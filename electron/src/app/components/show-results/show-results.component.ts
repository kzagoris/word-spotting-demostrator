import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CacheService } from '../../providers/cache.service';
import { ISearchResult } from '../../models/isearch-result';
import * as Clipper from 'image-clipper';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { StorageService } from '../../providers/storage.service';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-show-results',
  templateUrl: './show-results.component.html',
  styleUrls: ['./show-results.component.scss']
})
export class ShowResultsComponent implements OnInit {

  public resultsLength = 0;
  public results: Array<ISearchResult>;
  public resultsPaged: Array<ISearchResult>;
  selectedResult: ISearchResult | null;
  public displayResult: Subject<boolean> = new BehaviorSubject(false);
  public cols = 8;
  public Paging = 10;

  private collectionId: string;
  private documentId: string;

  constructor(public route: ActivatedRoute, public router: Router, public cache: CacheService,
    public storage: StorageService, public media: ObservableMedia) {
    this.selectedResult = null;

  }

  public showResultOnImage(result: ISearchResult) {
    this.selectedResult = result;

  }
  public goBack() {
    this.router.navigate(['/document/', this.collectionId, this.documentId]);
  }

  async ngOnInit() {
    const params = await this.route.params.first().toPromise();
    this.collectionId = params.collectionId;
    this.documentId = params.documentId;
    const collection = await this.storage.getDBCollection(params.collectionId);
    const documents = new Map<number, string>();
    for (const d of collection.documents) {
      documents.set(d.id, d.path);
    }
    try {
      this.results = await this.cache.getSearchResults(params.cacheId).first().toPromise();
    } catch {
      this.goBack();
      return;
    }
    if (this.results == null) {
      this.goBack();
      return;
    }
    this.resultsLength = this.results.length;
    if (this.resultsLength === 0) {
      this.goBack();
      return;
    }
    for (const result of this.results) {
      const imagePath = documents.get(result.imageId);
      result.content = Observable.defer(() =>
        this.getCropped(imagePath,
          result.location[0], result.location[1],
          result.location[2], result.location[3]))
        .publishReplay(1).refCount()
    }
    this.resultsPaged = this.results.slice(0, this.Paging);
    this.media.subscribe((m: MediaChange) => {
      switch (m.mqAlias) {
        case 'lg':
          this.cols = 6;
          break;
        case 'md':
          this.cols = 4;
          break;
        case 'sm':
          this.cols = 3;
          break;
        case 'xs':
          this.cols = 1;
          break;
      }
    });
    this.displayResult.next(true);
  }

  public async display(index: number, pageSize: number) {
    this.resultsPaged = this.results.slice(index * pageSize, (index + 1) * pageSize);
  }

  private async getCropped(imagePath: string, x: number, y: number, width: number, height: number) {
    const clip = await this.clipped(imagePath);
    return await this.crop(clip, x, y, width, height);
  }

  private clipped(imagePath: string) {
    return new Promise(function (resolve, reject) {
      Clipper(imagePath, function () {
        resolve(this);
      });
    });
  }

  private crop(clip: any, x: number, y: number, width: number, height: number): Promise<string> {
    return new Promise(function (resolve, reject) {
      clip.crop(x, y, width, height).toDataURL(resolve);
    });
  }

  returnToResults() {
    this.selectedResult = null;
  }
}
