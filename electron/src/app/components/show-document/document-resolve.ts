import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class DocumentResolve implements Resolve<{ title: string, link: string }> {

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ title: string; link: string }> | Promise<{ title: string; link: string }> | { title: string; link: string } {
    const collectionId = route.params['collectionId'];
    return {title: 'go back', link: "collection/" + collectionId};
  }

  constructor() {
  }

}
