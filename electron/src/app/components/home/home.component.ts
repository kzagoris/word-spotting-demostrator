import { ElectronService } from '../../providers/electron.service';
import {Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
  }


  constructor(public electronService: ElectronService) { }

  ngOnInit() {
  }

}
