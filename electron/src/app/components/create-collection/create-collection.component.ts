import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Ng2FileDropFiles} from 'app/ng2-file-drop';
import {ElectronService} from '../../providers/electron.service';
import {IDocument} from '../../models/idocument';
import {WsService} from '../../providers/ws.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/first';
import {StorageService} from '../../providers/storage.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {DialogConfirmationComponent} from '../dialog-confirmation/dialog-confirmation.component';
import {OptionsService} from '../../providers/options.service';
import {ICollectionOptions} from '../../models/icollection-options';
import {Subscription} from 'rxjs/Subscription';
import {ICollection} from '../../models/icollection';

@Component({
  selector: 'app-create-collection',
  templateUrl: './create-collection.component.html',
  styleUrls: ['./create-collection.component.scss']
})
export class CreateCollectionComponent implements OnInit, OnDestroy {

  public nameFormControl: FormControl;
  public documents: IDocument[] = [];
  public disabled = false;
  private options$: Subscription;
  public options: ICollectionOptions;
  // Supported image types
  // noinspection JSUnusedLocalSymbols
  private supportedFileTypes: string[] = ['image/png', 'image/jpeg', 'image/jpg', 'image/tif'];

  constructor(public electronService: ElectronService, public wsService: WsService, public storage: StorageService,
              public router: Router, public dialog: MatDialog, public optionsService: OptionsService) {
    this.nameFormControl = new FormControl({value: '', disabled: this.disabled}, [
      Validators.required
    ]);
    this.options$ = this.optionsService.getCollectionOptions().subscribe(value => this.options = value);
  }

  updateWindowSize(value) {
    this.options.WindowSize = parseInt(value);
    this.optionsService.updateCollectionOptions(this.options);
  }

  updateCoarsekQuantizationMaxIterations(value) {
    this.options.CoarsekQuantizationMaxIterations = parseInt(value);
    this.optionsService.updateCollectionOptions(this.options);
  }

  updateMinCCSize(value) {
    this.options.MinCCSize = parseInt(value);
    this.optionsService.updateCollectionOptions(this.options);
  }

  updateCoarketQuantizationTolerance(value) {
    this.options.CoarsekQuantizationTolerance = parseFloat(value);
    this.optionsService.updateCollectionOptions(this.options);
  }

  updateQY(value) {
    this.options.QY = parseInt(value);
    this.optionsService.updateCollectionOptions(this.options);
  }

  updateQX(value) {
    this.options.QX = parseInt(value);
    this.optionsService.updateCollectionOptions(this.options);
  }

  toggleDynamicWindow(value: boolean) {
    if (value) {
      this.options.WindowSize = -1;
      this.optionsService.updateCollectionOptions(this.options);
    } else {
      if (this.options.WindowSize === -1) {
        this.options.WindowSize = 68;
        this.optionsService.updateCollectionOptions(this.options);
      }
    }
  }

  updateQuantizationNumberLocalPoints(value) {
    this.options.QuantizationNumberLocalPoints = parseInt(value);
    this.optionsService.updateCollectionOptions(this.options);
  }

  updateCoarsekNumberCluster(value) {
    this.options.CoarsekNumberCluster = parseInt(value);
    this.optionsService.updateCollectionOptions(this.options);
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    this.options$.unsubscribe();
  }


  dragFilesDropped(dropedFiles: Ng2FileDropFiles) {
    dropedFiles.accepted.forEach(d => this.addDocument((d.file.path))
    );
  }

  async openDialog(type: 'openFile' | 'openDirectory') {
    const files = await this.electronService.selectDirectory(type);
    if (files) {
      files.forEach(f => this.addDocument(f));
    }

  }

  async createCollection() {
    const dialogRef = this.dialog.open(DialogConfirmationComponent, {
      width: '350px',
      data: {title: 'Create a Collection', message: 'Are you sure? This may take a while.'}
    });

    dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        this.disabled = true;
        const name = <string>this.nameFormControl.value;
        const collection: ICollection = {documents: this.documents, name, options: this.options};
        const collectionId = await this.wsService
          .createCollection(collection);
        collection.id = collectionId;
        await this.storage.createDBCollection(collection);
        await this.wsService.getCollectionListing(true);
        await this.router.navigate(['/collections']);
      }
    });

  }

  private addDocument(path: string) {
    const isDirectory = this.electronService.isDirectory(path);
    if (isDirectory) {
      this.electronService
        .getFilesInDirectory(path)
        .forEach(f => this._addDocument(f));
    } else {
      if (this.electronService.acceptedFileTypes(path)) {
        this._addDocument(path);
      }
    }
  }

  private _addDocument(file: string) {
    let maxId = this.documents.length > 0
      ? Math.max.apply(Math, this.documents.map(x => x.id)) + 1
      : 0;
    this.documents.push({id: maxId++, path: file})
  }


}
