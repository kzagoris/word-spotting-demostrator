import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ICollectionOptions} from '../models/icollection-options';
import {ISearchOptions} from '../models/isearch-options';

@Injectable()
export class OptionsService {

  private searchOptions$: BehaviorSubject<ISearchOptions>;
  private collectionOptions$: BehaviorSubject<ICollectionOptions>;

  constructor() {
    this.searchOptions$ = new BehaviorSubject<ISearchOptions>({
      TakeLocalPointsSameQuantizationBucket : 200,
      MultipleWordAssignments: 4,
      NumberCenterLocalPoints: 2
    });

    this.collectionOptions$ = new BehaviorSubject<ICollectionOptions>({
      WindowSize: -1,
      MinCCSize: 5,
      CoarsekQuantizationMaxIterations: 1000000,
      CoarsekQuantizationTolerance: 0.000001,
      QuantizationNumberLocalPoints: 2000000,
      CoarsekNumberCluster: 16,
      QX: 20,
      QY: 10
    });
  }

  public getSearchOptions = () => this.searchOptions$;
  public  getCollectionOptions= () => this.collectionOptions$.asObservable();

  public updateSearchOptions = (options: ISearchOptions) => this.searchOptions$.next(options);
  public updateCollectionOptions = (options: ICollectionOptions) => this.collectionOptions$.next(options);
}
