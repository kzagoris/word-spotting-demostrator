import {Injectable} from '@angular/core';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import {ipcRenderer, shell, remote} from 'electron';
import * as childProcess from 'child_process';
import * as fs from 'fs';
import * as nodePath from 'path';

@Injectable()
export class ElectronService {

  ipcRenderer: typeof ipcRenderer;
  childProcess: typeof childProcess;
  remote: typeof remote;
  fs: typeof fs;
  nodePath: typeof nodePath;

  constructor() {
    // Conditional imports
    if (this.isElectron()) {
      this.ipcRenderer = window.require('electron').ipcRenderer;
      this.childProcess = window.require('child_process');
      this.remote = window.require('electron').remote;
      this.fs = this.remote.require('fs');
      this.nodePath = this.remote.require('path');

    }
  }

  isElectron = () => {
    return window && window.process && window.process.type;
  };

  isDirectory = (path: string): boolean => this.fs.lstatSync(path).isDirectory();

  getFilesInDirectory = (path: string): string[] => this.fs.readdirSync(path)
    .filter(f => this.acceptedFileTypes(f))
    .map(f => path + '/' + f);

  acceptedFileTypes(file: string): boolean {
    return (this.nodePath.extname(file) === '.jpg'
      || this.nodePath.extname(file) === '.jpeg' || this.nodePath.extname(file) === '.png'
      || this.nodePath.extname(file) === '.tif');
  }

  getAppPath = () => this.remote.app.getAppPath();

  getUserDataPath = () => this.remote.app.getPath('userData');

  selectDirectory = (type: 'openFile' | 'openDirectory'): Promise<string[]> => {
    return new Promise<string[]>((resolve, reject) => {
      this.remote.dialog.showOpenDialog({
        title: 'Select a folder or files',
        properties: ['multiSelections', type],
        filters: [{name: 'Images', extensions: ['jpg', 'jpeg', 'png', 'tif']}]
      }, resolve);
    });
  };


  openBrowser = (url: string) => shell.openExternal(url);

}
