import {Injectable} from '@angular/core';
import {ISearchResult} from "../models/isearch-result";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/empty";
import {Subject} from "rxjs/Subject";
import {ReplaySubject} from "rxjs/ReplaySubject";

@Injectable()
export class CacheService {

  private cacheDictionary: Map<number, Subject<Array<ISearchResult>>>;
  private maxId: number;


  constructor() {
    this.cacheDictionary = new Map<number, Subject<Array<ISearchResult>>>();
    this.maxId = 0;
  }

  getSearchResults(cacheId: number): Observable<Array<ISearchResult>> {
    const id = parseInt(cacheId.toString());
    return this.cacheDictionary.has(id)
       ? this.cacheDictionary.get(id)
       : Observable.empty<Array<ISearchResult>>();
  }

  setSearchResults(results$: Observable<Array<ISearchResult>>):number {
    const currentId = this.maxId;
    let subject = new ReplaySubject<ISearchResult[]>(1);
    this.cacheDictionary.set(currentId, subject);
    results$.subscribe(r=> subject.next(r));
    this.maxId++;
    return currentId;
  }

}
