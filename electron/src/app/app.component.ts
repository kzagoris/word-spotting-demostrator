import { Component, OnInit } from "@angular/core";
import { ElectronService } from "./providers/electron.service";
import { OverlayContainer } from "@angular/cdk/overlay";
import "hammerjs";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import "rxjs/add/operator/map";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/mergeMap";
import { OptionsService } from "./providers/options.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  public title: string;
  public link: string;
  public activate: boolean;

  ngOnInit(): void {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map(route => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      })
      .filter(route => route.outlet === "primary")
      .mergeMap(route => route.data)
      .subscribe(data => {
        if (data.document) {
          this.activate = true;
          this.title = data.document.title;
          this.link = data.document.link;
        } else if (data.title) {
          this.activate = true;
          this.title = data.title;
          this.link = data.link;
        } else {
          this.activate = false;
        }
      });
  }

  constructor(
    public electronService: ElectronService,
    public overlayContainer: OverlayContainer,
    public router: Router,
    public activatedRoute: ActivatedRoute
  ) {
    overlayContainer.getContainerElement().classList.add("unicorn-dark-theme");

    if (electronService.isElectron()) {
      //console.log('Mode electron');
      // Check if electron is correctly injected (see externals in webpack.config.js)
      //console.log('c', electronService.ipcRenderer);
      // Check if nodeJs childProcess is correctly injected (see externals in webpack.config.js)
      //console.log('c', electronService.childProcess);
    } else {
      console.log("Mode web");
    }
  }

  open = (url: string) => this.electronService.openBrowser(url);
}
