import {ISearchOptions} from './isearch-options';

export interface IQuery {
  collectionId: number;
  queryImageBase64: string;
  top?: number;
  options: ISearchOptions;
}
