export interface ICollectionListing {
  id: number;
  name: string;
}
