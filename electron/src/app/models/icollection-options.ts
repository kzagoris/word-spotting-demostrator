export interface ICollectionOptions {
  WindowSize: number;
  MinCCSize: number;
  CoarsekQuantizationMaxIterations: number;
  CoarsekQuantizationTolerance: number;
  QuantizationNumberLocalPoints: number;
  CoarsekNumberCluster: number;
  QX: number;
  QY: number;
}
