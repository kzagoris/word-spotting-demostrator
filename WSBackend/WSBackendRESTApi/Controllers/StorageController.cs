﻿using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using WSBackendLibrary.Models;

namespace WSBackendRESTApi.Controllers
{
    [Route("api/[controller]")]
    public class StorageController : Controller
    {
        //GET api/storage
        [HttpGet("{id}")]
        public Collection Get(int Id) => WSBackendLibrary.Settings.Storage.Get(Id);

        //POST api/storage
        [HttpPost]
        public HttpResponseMessage Post([FromBody] Collection MyCollection)
        {
            WSBackendLibrary.Settings.Storage.Save(MyCollection);
            return new HttpResponseMessage { StatusCode = HttpStatusCode.NoContent };
        }

        // DELETE api/storage
        [HttpDelete("{id}")]
        public HttpResponseMessage Delete(int Id)
        {
            WSBackendLibrary.Settings.Storage.Delete(Id);
            return new HttpResponseMessage { StatusCode = HttpStatusCode.NoContent };
        }
    }
}
