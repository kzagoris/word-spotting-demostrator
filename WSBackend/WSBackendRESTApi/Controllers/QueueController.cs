﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WSBackendLibrary.Models;
using WSBackendLibrary.Interfaces;
using WSBackendLibrary;
using System.Net;
using System.Net.Http;



namespace WSBackendRESTApi.Controllers
{
    [Route("api/[controller]")]
    public class QueueController
    {
        private readonly IQueueTaskService _queueTask;

        public QueueController(IQueueTaskService queueTask)
        {
            this._queueTask = queueTask;
        }

        [HttpGet("complete/collection/{id}")]
        public int GetTask(int id)
        {
            bool? result = _queueTask.IsTaskComplete<int>(id);
            int r = -2;
            switch (result)
            {
                case null:
                    r = -2;
                    break;
                case true:
                    r = _queueTask.GetResult<int>(id);
                    break;
                case false:
                    r = -1;
                    break;
            }
            return r;
        }

        [HttpGet("collection/{id}")]
        public HttpResponseMessage GetTaskStatus(int id)
        {
            bool? result = _queueTask.IsTaskComplete<int>(id);
            var response = new HttpResponseMessage();
            if (result.HasValue)
            {
                if (result.Value)
                {
                    response.StatusCode = HttpStatusCode.SeeOther;
                    response.Headers.Location = new Uri($"http://localhost:5000/api/queue/complete/collection/{id}");

                }
                else
                {
                    response.StatusCode = HttpStatusCode.OK;
                    response.Content = new StringContent("PENDING");
                }
            }
            else
            {
                response.StatusCode = HttpStatusCode.NotFound;
                response.Content = new StringContent("TASK NOT FOUND");
            }

            return response;


        }
    }
}
