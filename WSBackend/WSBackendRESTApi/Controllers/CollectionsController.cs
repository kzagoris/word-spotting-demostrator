using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WSBackendLibrary.Models;
using WSBackendLibrary;
using WSBackendLibrary.Interfaces;
using System.Net;
using System.Net.Http;

namespace WSBackendRESTApi.Controllers
{
    [Route("api/[controller]")]
    public class CollectionsController : Controller
    {
        private readonly IQueueTaskService _queueTask;

        public CollectionsController(IQueueTaskService queueTask)
        {
            this._queueTask = queueTask;
        }

        //GET api/collections
        //Get all the collections id and name
        [HttpGet]
        public IEnumerable<CollectionListing> Get() => Collections.GetCollections();

        // GET api/collections/activate/list
        [HttpGet("activate/list/")]
        public IEnumerable<CollectionListing> ActivateList() => Collections.GetActiveCollections();

        // GET api/collections/activate/2
        // Load to Cache
        [HttpGet("activate/{id}")]
        public bool ActivateGet(int id)
        {
            Collections.ActivateCollection(id);
            return true;
        }



        [HttpGet("deactivate/{id}")]
        public bool DeActivateGet(int id)
        {
            Collections.DeActivateCollection(id);
            return true;
        }

        // POST api/collections
        // Create a Collection
        [HttpPost]
        public HttpResponseMessage Post([FromBody]Collection myCollection)
        {
            int taskId = this._queueTask.CreateTask<int>(() => Collections.CreateCollection(myCollection));
            var response = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Accepted
            };
            response.Headers.Location = new Uri($"http://localhost:5000/api/queue/collection/{taskId}");
            return response;
        }


        // PUT api/collections/5
        //add documents to the collection with id = {id}
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Document[] documents)
        {
            throw new NotImplementedException();
        }

        // DELETE api/collections/2
        [HttpDelete("{id}")]
        public HttpResponseMessage Delete(int id)
        {
            Collections.DeleteCollection(id);
            return new HttpResponseMessage() { StatusCode = HttpStatusCode.NoContent };
        }
    }
}
