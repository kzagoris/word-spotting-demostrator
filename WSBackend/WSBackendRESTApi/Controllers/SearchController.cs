using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WSBackendLibrary;
using WSBackendLibrary.Models;

namespace WSBackendRESTApi.Controllers
{
    [Route("api/[controller]")]
    public class SearchController : Controller
    {
        [HttpPost]
        public IEnumerable<Result> Post([FromBody]Query query)
        {
            if (query.Top <= 0) query.Top = 1000;
            if (query.Options == null) query.Options = new SearchOptions();
            var results = Search.Query(query.ImageBitmap, query.CollectionId, query.Options).Take(query.Top).Select(q => new Result()
            {
                ImageId = q.Document,
                Location = new int[] { q.X, q.Y, q.Width, q.Height }
            });
            return results;
        }

    }
}