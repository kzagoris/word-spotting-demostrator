﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoLF
{
    public class DoLF
    {

        public static byte[] TestImageData;
        public static int[] TestMatrixData;
        public static int TestImageWidth;
        public static int TestImageHeight;
        public static int Depth;

        /// <summary>
        /// The Document Specific Local Points
        /// </summary>
        public class DsLPoints
        {
            /// <summary>
            /// Window Size
            /// </summary>
            public int WindowSize { get; set; }

            /// <summary>
            /// Descriptor
            /// </summary>
            public float[] Descriptor { get; set; }

            public int X { get; set; }

            public int Y { get; set; }

            /// <summary>
            /// Quantization Gradient 
            /// </summary>
            public int Gradient { get; set; }

            public DsLPoints(int X, int Y)
            {
                this.X = X;
                this.Y = Y;
            }


        }

        public class Result
        {
            public int[] Block { get; set; }




            public int Position { get; set; }

            public float Similarity { get; set; }

            public Result Clone()
            {
                var newBlock = new int[Block.Length];
                Block.CopyTo(newBlock, 0);
                return new Result
                {
                    Position = Position,
                    Similarity = Similarity,
                    Block = newBlock
                };
            }
        }



        #region Settings

        public int WindowSize { get; set; }

        public int N { get; set; }

        public QuantizationLevelsNum QuantizationLevels { get; set; }

        /// <summary>
        /// Enable Dynamic Window
        /// </summary>
        public bool DynamicWindow { get; set; }

        public int MinCCSize { get; set; }

        #endregion

        public enum QuantizationLevelsNum
        {
            Two = 2,
            Three,
            Four,
            Five
        }

        ;



        /// <summary>
        /// Get the DSLP Local Points
        /// </summary>
        /// <param name="Image">an 24bit RGB Image</param>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        /// <param name="WordSpottingMethod"></param>
        /// <returns></returns>
        public DsLPoints[] GetDSLPoints(byte[] Image, int Width, int Height, int Depth, int WindowSize = 68, int N = 4, QuantizationLevelsNum QuantizationLevels = QuantizationLevelsNum.Four, int MinCCSize = 3, bool DynamicWindow = false)
        {

            this.WindowSize = WindowSize;
            this.N = N;
            this.QuantizationLevels = QuantizationLevels;
            this.MinCCSize = MinCCSize;
            this.DynamicWindow = DynamicWindow;
            var NewPixels = new byte[3 * Width * Height];
            int cstride = 3 * Width;
            int stride = Depth * Width;
            switch (Depth)
            {
                case 1:
                    for (int y = 0; y < Height; y++)
                        for (int x = 0; x < Width; x++)
                        {
                            NewPixels[y * cstride + 3 * x]
                                = NewPixels[y * cstride + 3 * x + 1]
                                = NewPixels[y * cstride + 3 * x + 2]
                                = Image[y * stride + Depth * x];
                        }
                    break;
                case 4:
                    for (int y = 0; y < Height; y++)
                        for (int x = 0; x < Width; x++)
                        {
                            NewPixels[y * cstride + 3 * x] = Image[y * stride + Depth * x];
                            NewPixels[y * cstride + 3 * x + 1] = Image[y * stride + Depth * x + 1];
                            NewPixels[y * cstride + 3 * x + 2] = Image[y * stride + Depth * x + 2];
                        }
                    break;
                case 3:
                    NewPixels = Image;
                    break;
                default:
                    throw new Exception("Not Supported Depth");

            }
            var myImage = new ZagImage<byte>(NewPixels, Width, Height, 3);
            var myDSLP = new DsLPsDetectorv2wFeatures
            {
                N = this.N,
                WindowSize = this.WindowSize,
                QuantizationLevels = this.QuantizationLevels,
                MinCCSize = this.MinCCSize,
                DynamicWindow = this.DynamicWindow
            };
            myDSLP.CalcDoLFs(myImage);
            return myDSLP.myDsLPs.Count() > 0 ? myDSLP.myDsLPs.ToArray() : new DsLPoints[0];
        }








    }
}
