﻿using System;
using System.Collections.Generic;
using System.IO;
using WSBackendLibrary.Models;

namespace WSBackendLibrary
{
    public class CustomBinaryFormat : IDisposable
    {
        private Dictionary<int, Descriptor> Cache;
        private FileStream FileStream;
        private const long Size = 72;
        // private byte[] ByteDescriptor;
        public object LockStream;

        public CustomBinaryFormat(string Datafile, FileMode fileMode, FileAccess fileAccess)
        {
            this.FileStream = new FileStream(Datafile, fileMode, fileAccess);
            // LockStream = new object();
            Cache = new Dictionary<int, Descriptor>();
            // ByteDescriptor = new byte[72];


        }
        /// <summary>
        /// Add a descriptors to the end of the stream and return its position which is a positive zero-based number. If it is a negative number, something went wrong
        /// </summary>
        /// <param name="D">The descriptors to add</param>
        /// <returns></returns>
        public int AddDescriptor(Descriptor D)
        {
            int position = -1;
            position = Convert.ToInt32(FileStream.Length / Size);
            FileStream.Seek(0, SeekOrigin.End);
            byte[] x = BitConverter.GetBytes(Convert.ToInt16(D.X));
            byte[] y = BitConverter.GetBytes(Convert.ToInt16(D.Y));
            byte[] document = BitConverter.GetBytes(D.Document);

            FileStream.Write(x, 0, 2);
            FileStream.Write(y, 0, 2);
            FileStream.Write(document, 0, 4);
            //fileStream.Write(BitConverter.GetBytes(D.Hashkey), 0, 8);
            FileStream.Write(D.Value, 0, 64);

            return position;
        }

        public Descriptor GetDescriptor(int Position)
        {
            if (Cache.TryGetValue(Position, out var cacheDescriptor))
                return cacheDescriptor;


            long start = Position * Size;

            FileStream.Seek(start, SeekOrigin.Begin);
            Span<byte> SpanDescriptor = stackalloc byte[72];
            this.FileStream.Read(SpanDescriptor);


            var myDescriptor = new Descriptor
            {
                Id = Position,
                X = BitConverter.ToInt16(SpanDescriptor.Slice(0, 2)),
                Y = BitConverter.ToInt16(SpanDescriptor.Slice(2, 2)),
                Document = BitConverter.ToInt32(SpanDescriptor.Slice(4, 4)),
                Value = SpanDescriptor.Slice(8, 64).ToArray()
            };
            Cache.Add(Position, myDescriptor);
            return myDescriptor;
        }

        public void Dispose()
        {
            FileStream.Close();
        }
    }
}
