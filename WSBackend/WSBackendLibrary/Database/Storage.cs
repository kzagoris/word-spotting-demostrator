﻿using System;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using Newtonsoft.Json;
using WSBackendLibrary.Models;

namespace WSBackendLibrary.Database
{
    public class Storage
    {
        private string _path;
        public Storage(string Path)
        {
            if (!Directory.Exists(Path)) Directory.CreateDirectory(Path);
            this._path = Path;
        }

        public bool Exists(int id) => Directory.Exists(Path.Combine(_path, id.ToString()));

        public void Delete(int id)
        {
            string path = Path.Combine(_path, $"{id}.json");
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }

        }

        public void Save(Collection MyCollection)
        {
            using (var sw = new StreamWriter(Path.Combine(this._path, $"{MyCollection.Id}.json")))
                sw.WriteLine(JsonConvert.SerializeObject(MyCollection));
        }

        public Collection Get(int id)
        {
            var myCollection = JsonConvert.DeserializeObject<Collection>(
              File.ReadAllText(Path.Combine(this._path, $"{id}.json"))
            );

            return myCollection;
        }
    }
}
