using Accord.MachineLearning;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using WSBackendLibrary.Models;

namespace WSBackendLibrary.Database
{
    public interface IDatabase
    {
        void SaveCollection(CollectionDatabase Collection);
        void DeleteCollection(int CollectionId);
        CollectionDatabase LoadCollection(int CollectionId);
        IEnumerable<CollectionListing> GetListCollection();
        bool CollectionExists(int CollectionId);
        void CreateDolfs(IEnumerable<Document> Documents, int CollectionId, CollectionOptions Options);
        int CreateCollection(string Name);
        double[][] GetDescriptors(Collection DataCollection);
        CollectionDatabase RunAssign(Collection DataCollection, double[][][] CoarsekClusters, double[][] ElementsClusters);
        void CreateThumbsImages(Collection DataCollection);
    }
}
