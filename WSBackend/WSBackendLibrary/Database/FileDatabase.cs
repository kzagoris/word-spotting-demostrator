using System;
using System.Collections.Generic;
using System.Text;
using WSBackendLibrary.Models;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Accord.MachineLearning;
using System.Collections.Concurrent;
using Accord.IO;
using SQLite;
using MoreLinq;
using System.Threading.Tasks;
using System.Threading;

namespace WSBackendLibrary.Database
{
    class FileDatabase : IDatabase
    {
        private string _path;
        private object CreateDolfLock;
        private object AssignLock;

        public FileDatabase(string Path)
        {
            if (!Directory.Exists(Path)) Directory.CreateDirectory(Path);
            this._path = Path;
            this.CreateDolfLock = new object();
            this.AssignLock = new object();
        }

        public bool CollectionExists(int id) => Directory.Exists(Path.Combine(_path, id.ToString()));

        public void DeleteCollection(int id)
        {
            if (Cache.Active.ContainsKey(id))
            {
                Cache.Active.TryRemove(id, out CollectionDatabase collection);
                collection.Dispose();

            }
            GC.Collect();
            GC.WaitForPendingFinalizers();
            string path = Path.Combine(_path, id.ToString());
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
        }

        public double[][] GetDescriptors(Collection DataCollection)
        {
            var enumeratorPoints = GetDsLPoints(DataCollection).GetEnumerator();
            var datasetPoints = new List<DoLF.DoLF.DsLPoints>();
            while (enumeratorPoints.MoveNext() && datasetPoints.Count() < DataCollection.Options.QuantizationNumberLocalPoints)
                datasetPoints.AddRange(enumeratorPoints.Current);
            return datasetPoints.AsParallel().Select(y => y.Descriptor.Select(z => (double)z).ToArray()).ToArray();
        }

        private IEnumerable<IEnumerable<DoLF.DoLF.DsLPoints>> GetDsLPoints(Collection DataCollection)
        {
            var randomDocuments = DataCollection.Documents.OrderBy(d => Guid.NewGuid());
            foreach (var doc in randomDocuments)
            {
                var wordImg = new NewImage(doc.Path);
                var myDolf = new DoLF.DoLF();
                var mypoints = myDolf.GetDSLPoints(wordImg.Data, wordImg.Width, wordImg.Height, 3,
                WindowSize: DataCollection.Options.WindowSize > 0 ? DataCollection.Options.WindowSize : 68,
                MinCCSize: DataCollection.Options.MinCCSize,
                DynamicWindow: DataCollection.Options.WindowSize == -1);
                int length = mypoints.Length;
                yield return mypoints.OrderBy(x => Guid.NewGuid()).Take((int)(0.5f * length));
            }

        }

        public void CreateDolfs(IEnumerable<Document> Documents, int CollectionId, CollectionOptions Options)
        {
            string databaseName = Path.Combine(_path, CollectionId.ToString(), "Dataset.db");
            if (File.Exists(databaseName)) File.Delete(databaseName);
            using (var db = new SQLiteConnection(databaseName))
            {
                db.CreateTable<DescriptorInfo>();
                Parallel.ForEach(Documents, new ParallelOptions() { }, doc =>
                {
                    var wordImg = new NewImage(doc.Path);
                    var myDolf = new DoLF.DoLF();
                    var mypoints = myDolf.GetDSLPoints(wordImg.Data, wordImg.Width, wordImg.Height, 3,
                    WindowSize: Options.WindowSize > 0 ? Options.WindowSize : 68,
                    MinCCSize: Options.MinCCSize,
                    DynamicWindow: Options.WindowSize == -1);
                    lock (CreateDolfLock)
                    {
                        mypoints.Select(w => new DescriptorInfo
                        {
                            DescriptorString = JsonConvert.SerializeObject(w.Descriptor),
                            X = w.X,
                            Y = w.Y,
                            Document = doc.Id
                        }).Batch(10000).ForEach(a => db.InsertAll(a));
                    }
                });
            }

        }

        public void CreateThumbsImages(Collection DataCollection)
        {
            //Directory.get
            string imagesThumbPath = Path.Combine(Path.GetDirectoryName(DataCollection.Documents[0].Path), "thumbs");

            if (!Directory.Exists(imagesThumbPath)) Directory.CreateDirectory(imagesThumbPath);
            //create thumbs
            foreach (var f in DataCollection.Documents)
            //Parallel.ForEach(DataCollection.Documents, f =>
            {
                var img = new NewImage(f.Path);
                var thumb = img.ResizeHQ(120f / img.Width);
                thumb.Save(Path.Combine(imagesThumbPath, Path.GetFileNameWithoutExtension(f.Path) + ".jpg"));
                thumb.Dispose();

            }
        }

        public CollectionDatabase RunAssign(Collection DataCollection, double[][][] CoarsekClusters, double[][] ElementsClusters)
        {
            string databaseQDBName = Path.Combine(_path, DataCollection.Id.ToString(), "DescriptorsDB.db");
            string datasetCBF = Path.Combine(_path, DataCollection.Id.ToString(), "LocalPoints.Data");

            if (File.Exists(databaseQDBName)) File.Delete(databaseQDBName);
            if (File.Exists(datasetCBF)) File.Delete(datasetCBF);
            var descriptorDb = new SQLiteConnection(databaseQDBName);
            var customBinFormat = new CustomBinaryFormat(datasetCBF, FileMode.CreateNew, FileAccess.Write);
            //Saving Options
            descriptorDb.CreateTable<CollectionOptions>();
            descriptorDb.Insert(DataCollection.Options);


            descriptorDb.CreateTable<HashDescriptor>();


            var InvertIndex = new ConcurrentBag<int>[(int)Math.Pow(DataCollection.Options.CoarsekNumberCluster, CoarsekClusters.Count())];
            for (int i = 0; i < InvertIndex.Length; i++)
                InvertIndex[i] = new ConcurrentBag<int>();



            //foreach (var doc in DataCollection.Documents)
            Parallel.ForEach(DataCollection.Documents, new ParallelOptions { MaxDegreeOfParallelism = 2 }, doc =>
            {
                var wordImg = new NewImage(doc.Path);
                var myDolf = new DoLF.DoLF();
                var mypoints = myDolf.GetDSLPoints(wordImg.Data, wordImg.Width, wordImg.Height, 3,
             WindowSize: DataCollection.Options.WindowSize > 0 ? DataCollection.Options.WindowSize : 68,
             MinCCSize: DataCollection.Options.MinCCSize,
             DynamicWindow: DataCollection.Options.WindowSize == -1);
                var hashDescriptors = new ConcurrentBag<HashDescriptor>();
                //foreach (var point in mypoints)
                Parallel.ForEach(mypoints, point =>
                {

                    int X = point.X / DataCollection.Options.QX;
                    int Y = point.Y / DataCollection.Options.QY;
                    var hashKey = GetHashCode(X, Y, doc.Id);
                    double[] value = point.Descriptor.Select(x => (double)x).ToArray();
                    int coarsekIndex = Quantization.GetCoarsekIndex(value, CoarsekClusters, DataCollection.Options);
                    byte[] qValue = value.Select(d => Decide(ElementsClusters, d)).ToArray();
                    var myDescriptor = new Descriptor
                    {
                        X = X,
                        Y = Y,
                        Document = doc.Id,
                        Value = qValue,
                        Hashkey = hashKey
                    };

                    lock (AssignLock)
                    {
                        myDescriptor.Id = customBinFormat.AddDescriptor(myDescriptor);
                        if (myDescriptor.Id < 0) throw new Exception("descriptor id returned is negative");
                    }
                    InvertIndex[coarsekIndex].Add(myDescriptor.Id);
                    hashDescriptors.Add(new HashDescriptor
                    {
                        Id = myDescriptor.Id,
                        Hashkey = hashKey
                    });
                });
                lock (AssignLock)
                {
                    descriptorDb.InsertAll(hashDescriptors);
                }

            });
            descriptorDb.Close();
            descriptorDb.Dispose();
            customBinFormat.Dispose();
            return new CollectionDatabase(DataCollection.Id, InvertIndex.Select(x => x.ToArray()).ToArray(), CoarsekClusters, ElementsClusters);
        }

        private byte Decide(double[][] ElementsClusters, double d)
        {
            int center = 0; double value = (ElementsClusters[0][0] - d) * (ElementsClusters[0][0] - d);
            for (int i = 1; i < ElementsClusters.Length; i++)
            {
                var v = (ElementsClusters[i][0] - d) * (ElementsClusters[i][0] - d);
                if (v < value)
                {
                    value = v;
                    center = i;
                }
            }
            return (byte)center;
        }


        private long GetHashCode(int X, int Y, int D) => X + (501 * Y) + (501 * 501 * D);



        public IEnumerable<CollectionListing> GetListCollection()
        {
            return Directory.GetDirectories(this._path)
                .Select(d => new CollectionListing(Convert.ToInt32(Path.GetFileName(d)), File.ReadAllText(Path.Combine(d, "name"))));
        }

        public CollectionDatabase LoadCollection(int id)
        {
            var myCollection = new CollectionDatabase
            {
                Id = id,
                Name = File.ReadAllText(Path.Combine(_path, id.ToString(), "name"))
            };

            var jsonSerialization = new JsonSerializer();
            using (var sw = new StreamReader(Path.Combine(_path, id.ToString(), "InvertIndex.json")))
            using (var reader = new JsonTextReader(sw))
                myCollection.InvertIndex = jsonSerialization.Deserialize<int[][]>(reader);


            myCollection.DescriptorsDB = new SQLiteConnection(Path.Combine(_path, id.ToString(), "DescriptorsDB.db"));
            myCollection.CBF = new CustomBinaryFormat(Path.Combine(_path, id.ToString(), "LocalPoints.Data"), FileMode.Open, FileAccess.Read);
            myCollection.Options = myCollection.DescriptorsDB.Table<CollectionOptions>().Single();

            myCollection.CoarsekClusters = JsonConvert.DeserializeObject<double[][][]>(File.ReadAllText(Path.Combine(_path, id.ToString(), "CoarsekClusters.json")));
            myCollection.ElementsClusters = JsonConvert.DeserializeObject<double[][]>(File.ReadAllText(Path.Combine(_path, id.ToString(), "ElementsClusters.json")));





            return myCollection;
        }


        public int CreateCollection(string Name)
        {
            int id = GetNewCollectionId();
            string collectionPath = Path.Combine(_path, id.ToString());
            if (!Directory.Exists(collectionPath)) Directory.CreateDirectory(collectionPath);
            File.WriteAllText(Path.Combine(collectionPath, "name"), Name);
            return id;
        }

        public void SaveCollection(CollectionDatabase Collection)
        {
            string collectionPath = Path.Combine(_path, Collection.Id.ToString());
            if (!Directory.Exists(collectionPath)) Directory.CreateDirectory(collectionPath);


            using (var sw = new StreamWriter(Path.Combine(collectionPath, "InvertIndex.json")))
                sw.WriteLine(JsonConvert.SerializeObject(Collection.InvertIndex));

            File.WriteAllText(Path.Combine(collectionPath, "CoarsekClusters.json"), JsonConvert.SerializeObject(Collection.CoarsekClusters));
            File.WriteAllText(Path.Combine(collectionPath, "ElementsClusters.json"), JsonConvert.SerializeObject(Collection.ElementsClusters));

            if (!File.Exists(Path.Combine(collectionPath, "name"))) File.WriteAllText(Path.Combine(collectionPath, "name"), Collection.Name);
        }

        private int GetNewCollectionId()
        {
            var files = Directory.GetDirectories(_path);
            if (files.Length == 0) return 1;
            return files.Select(d => Convert.ToInt32(Path.GetFileName(d))).Max() + 1;
        }


    }
}
