using Accord.MachineLearning;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSBackendLibrary.Models;
using Accord.Math;
using MoreLinq;

namespace WSBackendLibrary
{
    class Quantization
    {
        public static (double[][][] Coarsek, double[][] Elements) Run(Collection DataCollection)
        {
            double[][] descriptors = Settings.Database.GetDescriptors(DataCollection);
            double[][][] coarsekClusters = null;
            double[][] elementsClusters = null;
            Parallel.Invoke(
                () => coarsekClusters = MultipleCoarsek(descriptors, DataCollection.Options),
                () => elementsClusters = ElementsClustering(descriptors, DataCollection.Options));
            return (coarsekClusters, elementsClusters);

        }


        public static double[][] Coarsek(double[][] Descriptors, CollectionOptions Options)
        {
            var kmeans = new KMeans(Options.CoarsekNumberCluster)
            {
                MaxIterations = Options.CoarsekQuantizationMaxIterations,
                Tolerance = Options.CoarsekQuantizationTolerance,
                ComputeCovariances = false,
                ComputeError = false
            };
            KMeansClusterCollection clusters = kmeans.Learn(Descriptors);
            return clusters.Centroids;
        }

        public static double[][][] MultipleCoarsek(double[][] Descriptors, CollectionOptions Options)
        {
            var multipleCoarseks = new double[4][][];

            Parallel.For(0, 4, m =>
            {
                var descriptors = new double[Descriptors.Length][];
                for (int n = 0; n < Descriptors.Length; n++)
                    descriptors[n] = Transform(Descriptors[n], m);
                multipleCoarseks[m] = Coarsek(descriptors, Options);
            });
            return multipleCoarseks;
        }

        public static T[] Transform<T>(T[] Descriptor, int m)
        {
            int offset = m;
            if (m == 2 || m == 3) offset = m + 2;
            return new T[] {
            Descriptor[offset*8], Descriptor[offset*8 + 1], Descriptor[offset*8 + 2], Descriptor[offset*8 + 3],
            Descriptor[offset*8+4], Descriptor[offset*8 + 5], Descriptor[offset*8 + 6], Descriptor[offset*8 + 7],
            Descriptor[offset*8+16], Descriptor[offset*8 + 17], Descriptor[offset*8 + 18], Descriptor[offset*8 + 19],
            Descriptor[offset*8+20], Descriptor[offset*8 + 21], Descriptor[offset*8 + 22], Descriptor[offset*8 + 23],
          };
        }

        public static int GetCoarsekIndex(double[] descriptor, double[][][] CoarsekClusters, CollectionOptions Options)
        {
            var coarsekIndexes = new int[CoarsekClusters.Length];
            for (int m = 0; m < CoarsekClusters.Length; m++)
            {
                coarsekIndexes[m] = Decide(CoarsekClusters[m], Transform(descriptor, m));
            }
            return coarsekIndexes[0] * Options.CoarsekNumberCluster * Options.CoarsekNumberCluster * Options.CoarsekNumberCluster
                  + coarsekIndexes[1] * Options.CoarsekNumberCluster * Options.CoarsekNumberCluster
                  + coarsekIndexes[2] * Options.CoarsekNumberCluster
                  + coarsekIndexes[3];
        }

        static double GetEuclideanCode(double[] word, double[] query)
        {
            double distance = 0;
            for (int d = 0; d < word.Length; d++)
                distance += (word[d] - query[d]) * (word[d] - query[d]);
            return distance;
        }

        static int Decide(double[][] centroids, double[] query)
        {
            int center = 0; double value = GetEuclideanCode(centroids[0], query);
            for (int i = 1; i < centroids.Length; i++)
            {
                var v = GetEuclideanCode(centroids[i], query);
                if (v < value)
                {
                    value = v;
                    center = i;
                }
            }
            return center;
        }

        public static int GetCoarsekIndex(int m0, int m1, int m2, int m3, int CoarsekNumberCluster) =>
           m0 * CoarsekNumberCluster * CoarsekNumberCluster * CoarsekNumberCluster
                + m1 * CoarsekNumberCluster * CoarsekNumberCluster
                + m2 * CoarsekNumberCluster
                + m3;

        public static double[][] ElementsClustering(double[][] Descriptors, CollectionOptions Options)
        {
            var kmeans = new KMeans(256)
            {
                MaxIterations = Options.CoarsekQuantizationMaxIterations * 10,
                ComputeCovariances = false,
                ComputeError = false
            };
            KMeansClusterCollection clusters = kmeans.Learn(Descriptors);
            return clusters.Centroids;
        }



    }
}
