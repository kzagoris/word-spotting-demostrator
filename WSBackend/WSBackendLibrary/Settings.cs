using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using WSBackendLibrary.Database;
using WSBackendLibrary.Models;

namespace WSBackendLibrary
{
    public static class Settings
    {
        public static IDatabase Database = new FileDatabase(Path.GetFullPath("./databases"));
        public static Storage Storage = new Storage(Path.GetFullPath("./storage"));

    }
}
