﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WSBackendLibrary.Interfaces;

namespace WSBackendLibrary.Models
{
    public class QueueTasks : IQueueTaskService, IDisposable
    {
        private ConcurrentDictionary<int, object> _tasks;

        public QueueTasks()
        {
            this._tasks = new ConcurrentDictionary<int, object>();
        }

        public int CreateTask<T>(Func<T> process)
        {
            var myTask = Task.Run<T>(process);
            this._tasks.TryAdd(myTask.Id, myTask);
            return myTask.Id;
        }

        public bool? IsTaskComplete<T>(int TaskId) => _tasks.ContainsKey(TaskId) ? (bool?)((Task<T>)_tasks[TaskId]).IsCompleted : null;


        public Task<T> GetTask<T>(int TaskId)
        {
            if (_tasks.ContainsKey(TaskId)) return _tasks[TaskId] as Task<T>;
            throw new KeyNotFoundException("The task id is not found in the queue");
        }

        public T GetResult<T>(int TaskId)
        {
            if (_tasks.ContainsKey(TaskId))
            {
                var task = _tasks[TaskId] as Task<T>;
                if (task.IsCompleted)
                    return task.Result;
                throw new InvalidOperationException("The Task is not completed");
            }
            throw new KeyNotFoundException("The task id is not found in the queue");
        }

        public void Dispose()
        {
            foreach (var key in _tasks.Keys)
            {
                var myTask = (Task)_tasks[key];
                myTask.Dispose();
            }
        }
    }
}
