using System;
using System.Drawing;
using System.IO;

namespace WSBackendLibrary.Models
{
    public class Query
    {
        public int CollectionId { get; set; }
        public string QueryImageBase64 { get; set; }
        public int Top { get => _top; set => _top = value; }
        public SearchOptions Options { get; set; }
        private int _top = 1000;
        public NewImage ImageBitmap
        {
            get
            {
                // Convert Base64 String to byte[]
                byte[] imageBytes = Convert.FromBase64String(this.QueryImageBase64);
                MemoryStream ms = new MemoryStream(imageBytes, 0,
                  imageBytes.Length);

                // Convert byte[] to Image
                ms.Write(imageBytes, 0, imageBytes.Length);
                Image image = Image.FromStream(ms, true);
                return new NewImage((Bitmap)image);
            }

        }

    }
}