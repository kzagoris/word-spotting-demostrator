﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSBackendLibrary.Models
{
    public class SearchOptions
    {
        public int TakeLocalPointsSameQuantizationBucket { get; set; }
        public int MultipleWordAssignments { get; set; }
        public int NumberCenterLocalPoints { get; set; }
        public SearchOptions()
        {
            this.TakeLocalPointsSameQuantizationBucket = 300;
            this.MultipleWordAssignments = 4;
            this.NumberCenterLocalPoints = 1;
        }

    }
}
