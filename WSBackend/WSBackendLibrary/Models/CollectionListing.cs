﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSBackendLibrary.Models
{
    public class CollectionListing
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public CollectionListing() { }
        public CollectionListing(int Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
        }
    }
}
