﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace WSBackendLibrary.Models
{
    class HashDescriptor
    {
        [PrimaryKey, Indexed, NotNull]
        public int Id { get; set; }
        [Indexed, NotNull]
        public long Hashkey { get; set; }

    }
}
