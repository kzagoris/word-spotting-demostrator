﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace WSBackendLibrary.Models
{
    class DescriptorInfo
    {
        [PrimaryKey, AutoIncrement, Indexed]
        public int Id { get; set; }
        public string DescriptorString { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        [Indexed]
        public int Document { get; set; }

        private float[] descriptor;
        [Ignore]
        public float[] Descriptor
        {
            get
            {
                if (descriptor == null) descriptor = JsonConvert.DeserializeObject<float[]>(DescriptorString);
                return descriptor;
            }
        }
    }
}
