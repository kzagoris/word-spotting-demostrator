namespace WSBackendLibrary.Models
{
    public class Collection
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Document[] Documents { get; set; }
        public CollectionOptions Options { get; set; }
    }
}
