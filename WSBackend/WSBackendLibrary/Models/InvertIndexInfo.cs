﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSBackendLibrary.Models
{
    [Serializable]
    public class InvertIndexInfo
    {
        public int D { get; set; } // document index
        public int X { get; set; }
        public int Y { get; set; }
        //public int[] Code { get; set; }
        public float[] Descriptor { get; set; }
    }
}
