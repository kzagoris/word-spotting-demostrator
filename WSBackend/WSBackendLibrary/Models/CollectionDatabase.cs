using Accord.MachineLearning;
using SQLite;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace WSBackendLibrary.Models
{
    public class CollectionDatabase : IDisposable
    {
        public int[][] InvertIndex { get; set; }
        public double[][][] CoarsekClusters { get; set; }
        public double[][] ElementsClusters { get; set; }

        public SQLiteConnection DescriptorsDB { get; set; }

        public CustomBinaryFormat CBF { get; set; }

        public CollectionOptions Options { get; set; }


        public int Id { get; set; }
        public string Name { get; set; }

        public CollectionDatabase(int Id, int[][] InvertIndex, double[][][] CoarsekClusters, double[][] ElementsClusters)
        {
            this.Id = Id;
            this.InvertIndex = InvertIndex;
            this.CoarsekClusters = CoarsekClusters;
            this.ElementsClusters = ElementsClusters;
        }

        public CollectionDatabase() { }

        public void Dispose()
        {
            DescriptorsDB?.Dispose();
            CBF?.Dispose();
        }
    }
}
