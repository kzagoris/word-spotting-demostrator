﻿using System;
using System.Collections.Generic;
using System.Text;


namespace WSBackendLibrary.Models
{

    public class Rectangle
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public float Distance { get; set; }
        public int Document { get; set; }

        public int Area { get => this.Width * this.Height; }

        public Rectangle Intersect(Rectangle B)
        {
            var x = Math.Max(this.X, B.X);
            var num1 = Math.Min(this.X + this.Width, B.X + B.Width);
            var y = Math.Max(this.Y, B.Y);
            var num2 = Math.Min(this.Y + this.Height, B.Y + B.Height);

            return (num1 >= x && num2 >= y)
                ? new Rectangle { X = x, Y = y, Width = num1 - x, Height = num2 - y, Distance = Math.Max(this.Distance, B.Distance) }
                : null;
        }

        public Rectangle Union(Rectangle B)
        {
            var x1 = Math.Min(this.X, B.X);
            var x2 = Math.Max(this.X + this.Width, B.X + B.Width);
            var y1 = Math.Min(this.Y, B.Y);
            var y2 = Math.Max(this.Y + this.Height, B.Y + B.Height);
            return new Rectangle { X = x1, Y = y1, Width = x2 - x1, Height = y2 - y1 };
        }
    }
}
