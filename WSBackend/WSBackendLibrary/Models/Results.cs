namespace WSBackendLibrary.Models
{
    public class Result
    {
        public int ImageId { get; set; }
        public int[] Location { get; set; } // x, ,y ,width, height
    }
}