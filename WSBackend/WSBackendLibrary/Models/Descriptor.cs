﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace WSBackendLibrary.Models
{
    public class Descriptor
    {
        [PrimaryKey, Indexed, NotNull]
        public int Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        [Indexed, NotNull]
        public long Hashkey { get; set; }
        public int Document { get; set; } // document index
        public byte[] Value { get; set; }
    }
}
