﻿namespace WSBackendLibrary.Models
{
    public class CollectionOptions
    {
        public int WindowSize { get; set; }
        public int MinCCSize { get; set; }
        public int CoarsekQuantizationMaxIterations { get; set; }
        public double CoarsekQuantizationTolerance { get; set; }
        public int QuantizationNumberLocalPoints { get; set; }
        public int CoarsekNumberCluster { get; set; }
        public int QX { get; set; }
        public int QY { get; set; }

        public CollectionOptions()
        {
            this.WindowSize = -1;
            this.MinCCSize = 5;
            this.CoarsekQuantizationMaxIterations = 1000000;
            this.CoarsekQuantizationTolerance = 0.000001;
            this.QuantizationNumberLocalPoints = 2000000;
            this.CoarsekNumberCluster = 16;
            this.QX = 20;
            this.QY = 10;
        }
    }
}
