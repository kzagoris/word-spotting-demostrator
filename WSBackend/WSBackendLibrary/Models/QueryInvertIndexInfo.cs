using System;
using System.Collections.Generic;

[Serializable]
public class QueryInvertIndexInfo
{
    public List<int> CoarsetLabels { get; set; } // word index
    public int X { get; set; }
    public int Y { get; set; }
    //public double[] Residual { get; set; }
    public float[] Descriptor { get; set; }
}