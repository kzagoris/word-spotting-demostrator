﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSBackendLibrary
{
    class MaxHeap<T>
    {
        private int maximumCapacity;
        private SortedList<double, T> sortedList;
        public MaxHeap(int MaximumCapacity)
        {
            this.maximumCapacity = MaximumCapacity;
            this.sortedList = new SortedList<double, T>(maximumCapacity + 2);
        }

        public void AddItem(double Distance, T Value)
        {
            if (sortedList.Count() > maximumCapacity)
                if (Distance > sortedList.Keys[sortedList.Count() - 1])
                    return;
            this.sortedList.Add(Distance, Value);
            if (this.sortedList.Count() > maximumCapacity)
                this.sortedList.RemoveAt(this.sortedList.Count() - 1);
        }

        public bool ContainsKey(double Distance) => this.sortedList.ContainsKey(Distance);

        public List<T> GetList() => sortedList.Values.ToList();


    }
}
