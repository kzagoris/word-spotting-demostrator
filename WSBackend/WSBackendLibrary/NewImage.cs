using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSBackendLibrary
{
    public class NewImage
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public byte[] Data { get; set; }

        public NewImage(Bitmap Image)
        {
            var newimage = ConvertBitmapToNewImage(Image);
            this.Data = newimage.Data;
            this.Width = newimage.Width;
            this.Height = newimage.Height;
        }

        public NewImage(string ImagePath)
        {
            using (var img = (Bitmap)Image.FromFile(ImagePath))
            {
                var newimage = ConvertBitmapToNewImage(img);
                this.Data = newimage.Data;
                this.Width = newimage.Width;
                this.Height = newimage.Height;
            }

        }

        private NewImage() { }

        private NewImage ConvertBitmapToNewImage(Bitmap img)
        {
            var myImg = new NewImage
            {
                Width = img.Width,
                Height = img.Height
            };
            byte[] pixels = null;
            BitmapData srcData = img.LockBits(new Rectangle(0, 0, img.Width, img.Height), ImageLockMode.ReadOnly, img.PixelFormat);
            int stride = srcData.Stride;
            int depth = 3;
            switch (img.PixelFormat)
            {
                case PixelFormat.Format24bppRgb:
                    depth = 3;
                    break;
                case PixelFormat.Format32bppArgb:
                    depth = 4;
                    break;
                case PixelFormat.Format8bppIndexed:
                default:
                    throw new Exception("Image Format Not Supported!");

            }

            pixels = new byte[3 * img.Width * img.Height];

            int nstride = 3 * img.Width;
            unsafe
            {
                var src = (byte*)srcData.Scan0.ToPointer();
                for (int y = 0; y < img.Height; y++)
                    for (int x = 0; x < img.Width; x++)
                    {
                        pixels[y * nstride + 3 * x] = src[y * stride + depth * x];
                        pixels[y * nstride + 3 * x + 1] = src[y * stride + depth * x + 1];
                        pixels[y * nstride + 3 * x + 2] = src[y * stride + depth * x + 2];
                    }
            }
            myImg.Data = pixels;
            return myImg;
        }

        public Bitmap GetBitmap()
        {
            var imageData2 = new byte[4 * Width * Height];

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                {
                    imageData2[y * 4 * Width + 4 * x] = Data[y * 3 * Width + 3 * x];
                    imageData2[y * 4 * Width + 4 * x + 1] = Data[y * 3 * Width + 3 * x + 1];
                    imageData2[y * 4 * Width + 4 * x + 2] = Data[y * 3 * Width + 3 * x + 2];

                    imageData2[y * 4 * Width + 4 * x + 3] = 255;
                }



            Bitmap img;
            unsafe
            {
                fixed (byte* ptr = imageData2)
                {
                    img = new Bitmap(Width, Height, Width * 4,
                            PixelFormat.Format32bppArgb, new IntPtr(ptr));
                }
            }
            return img;
        }

        public void Save(string fileName)
        {
            var image = GetBitmap();
            image.Save(fileName);
            image.Dispose();
        }

        public Bitmap ResizeHQ(float scale)
        {
            var newWidth = (int)Math.Round(scale * Width);
            var newHeight = (int)Math.Round(scale * Height);
            var destRect = new Rectangle(0, 0, newWidth, newHeight);
            var destImage = new Bitmap(newWidth, newHeight);


            using (var img = GetBitmap())
            {
                destImage.SetResolution(img.HorizontalResolution, img.VerticalResolution);
                using (var graphics = Graphics.FromImage(destImage))
                {
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    using (var wrapMode = new ImageAttributes())
                    {
                        wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                        graphics.DrawImage(img, destRect, 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, wrapMode);
                    }
                }
            }

            return destImage;
        }
    }


}
