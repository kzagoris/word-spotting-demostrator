using Accord.MachineLearning;
using System;
using System.Collections.Concurrent;
using System.Linq;
using WSBackendLibrary.Models;
using WSBackendLibrary.Database;
using System.Collections.Generic;
using System.Threading;

namespace WSBackendLibrary
{
    public class Collections
    {

        public static int CreateCollection(Collection DataCollection)
        {
            if (DataCollection.Options == null) DataCollection.Options = new CollectionOptions();
            DataCollection.Id = Settings.Database.CreateCollection(DataCollection.Name);

            Settings.Database.CreateThumbsImages(DataCollection);

            (double[][][] coarsekClusters, double[][] elementsClusters) = Quantization.Run(DataCollection);
            CollectionDatabase collection = Settings.Database.RunAssign(DataCollection, coarsekClusters, elementsClusters);
            collection.Name = DataCollection.Name;
            Settings.Database.SaveCollection(collection);
            return collection.Id;
        }

        public static IEnumerable<CollectionListing> GetCollections() => Settings.Database.GetListCollection();

        public static void DeleteCollection(int id) => Settings.Database.DeleteCollection(id);


        public static void Cleanup() => Cache.Active.Clear();

        public static IEnumerable<CollectionListing> GetActiveCollections() => Cache.Active.Select(c => new CollectionListing(c.Key, c.Value.Name));

        public static void ActivateCollection(int id)
        {
            if (!Settings.Database.CollectionExists(id)) throw new KeyNotFoundException("Collection does not exist");
            if (!Cache.Active.ContainsKey(id))
            {
                var collection = Settings.Database.LoadCollection(id);
                Cache.Active.TryAdd(id, collection);
            }
        }


        public static void DeActivateCollection(int id)
        {
            if (!Settings.Database.CollectionExists(id)) throw new KeyNotFoundException("Collection does not exist");
            if (Cache.Active.ContainsKey(id))
            {
                Cache.Active.TryRemove(id, out CollectionDatabase value);
                value.Dispose();
            }
        }



    }
}
