using System;
using System.Collections.Generic;
using System.Text;
using WSBackendLibrary.Models;
using WSBackendLibrary.Database;
using System.Linq;
using MoreLinq;
using static DoLF.DoLF;
using Accord.MachineLearning;
using Accord.Math;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace WSBackendLibrary
{
    public class Search
    {
        private static ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentBag<Descriptor>>>> PositionIndex =
            new ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentBag<Descriptor>>>>();
        private static ConcurrentDictionary<long, bool> HashCache = new ConcurrentDictionary<long, bool>();
        public static IEnumerable<Rectangle> Query(NewImage QueryImage, in int CollectionId, SearchOptions Options)
        {
            var myCollection = Cache.Active.ContainsKey(CollectionId) ? Cache.Active[CollectionId] : Settings.Database.LoadCollection(CollectionId);
            if (!Cache.Active.ContainsKey(CollectionId)) Cache.Active.TryAdd(CollectionId, myCollection);


            int starts = Options.NumberCenterLocalPoints;
            var Query = GetQueryPoints(QueryImage, myCollection.CoarsekClusters, myCollection.ElementsClusters, Options.MultipleWordAssignments, myCollection.Options);
            if (Query.Length == 0) return new List<Rectangle>();
            if (Query.Length < starts) starts = Query.Length;
            var PositionDifferenceX = new short[starts, Query.Length];
            var PositionDifferenceY = new short[starts, Query.Length];
            for (int s = 0; s < starts; s++)
            {
                for (int i = 0; i < Query.Length; i++)
                {
                    PositionDifferenceX[s, i] = (short)(Query[i].X - Query[s].X);
                    PositionDifferenceY[s, i] = (short)(Query[i].Y - Query[s].Y);
                }
            }
            var InvertIndexesFromQuery = new Descriptor[starts][];
            //Parallel.For(0, starts, s =>
            lock (myCollection.CBF.LockStream)
                for (var s = 0; s < starts; s++)
                {
                    var localpoints = new List<int>();

                    Query[s].CoarsetLabels.ForEach(q => localpoints.AddRange(myCollection.InvertIndex[q]));
                    InvertIndexesFromQuery[s] = GetDescriptorsFromDatasetUsingCBF(localpoints, myCollection);
                }



            var FinalResults = new ConcurrentBag<Rectangle>();
            for (int s = 0; s < starts; s++)
            //Parallel.For(0, starts, s =>
            {
                if (InvertIndexesFromQuery[s] == null) return new List<Rectangle>();
                if (InvertIndexesFromQuery[s].Count() == 0) return new List<Rectangle>();
                var distances = new List<(float Distance, Descriptor info)>();
                foreach (var invertIndexFromQuery in InvertIndexesFromQuery[s])
                {
                    var distance = GetEuclideanCode(invertIndexFromQuery.Value, Query[s].Descriptor, myCollection.ElementsClusters);
                    distances.Add((distance, invertIndexFromQuery));
                }
                var orderedDistances = distances.PartialSortBy(Options.TakeLocalPointsSameQuantizationBucket, x => x.Distance);
                var localPointsLocations = new List<long>();
                // Find the localpoints to get
                foreach (var (Distance, info) in orderedDistances)
                {
                    for (int q = 0; q < Query.Length; q++)
                    {
                        int positionX = info.X + PositionDifferenceX[s, q];
                        int positionY = info.Y + PositionDifferenceY[s, q];
                        var _hash = GetHashCode(positionX, positionY, info.Document);
                        if (!HashCache.ContainsKey(_hash))
                        {
                            HashCache.TryAdd(_hash, default);
                            localPointsLocations.Add(_hash);
                        }
                        _hash = GetHashCode(positionX - 1, positionY, info.Document);
                        if (!HashCache.ContainsKey(_hash))
                        {
                            HashCache.TryAdd(_hash, default);
                            localPointsLocations.Add(_hash);
                        }
                        _hash = GetHashCode(positionX + 1, positionY, info.Document);
                        if (!HashCache.ContainsKey(_hash))
                        {
                            HashCache.TryAdd(_hash, default);
                            localPointsLocations.Add(_hash);
                        }
                        _hash = GetHashCode(positionX, positionY - 1, info.Document);
                        if (!HashCache.ContainsKey(_hash))
                        {
                            HashCache.TryAdd(_hash, default);
                            localPointsLocations.Add(_hash);
                        }
                        _hash = GetHashCode(positionX - 1, positionY - 1, info.Document);
                        if (!HashCache.ContainsKey(_hash))
                        {
                            HashCache.TryAdd(_hash, default);
                            localPointsLocations.Add(_hash);
                        }
                        _hash = GetHashCode(positionX + 1, positionY - 1, info.Document);
                        if (!HashCache.ContainsKey(_hash))
                        {
                            HashCache.TryAdd(_hash, default);
                            localPointsLocations.Add(_hash);
                        }
                        _hash = GetHashCode(positionX, positionY + 1, info.Document);
                        if (!HashCache.ContainsKey(_hash))
                        {
                            HashCache.TryAdd(_hash, default);
                            localPointsLocations.Add(_hash);
                        }
                        _hash = GetHashCode(positionX - 1, positionY + 1, info.Document);
                        if (!HashCache.ContainsKey(_hash))
                        {
                            HashCache.TryAdd(_hash, default);
                            localPointsLocations.Add(_hash);
                        }
                        _hash = GetHashCode(positionX + 1, positionY + 1, info.Document);
                        if (!HashCache.ContainsKey(_hash))
                        {
                            HashCache.TryAdd(_hash, default);
                            localPointsLocations.Add(_hash);
                        }
                    }
                }

                GetDescriptorsFromDatasetHashCode2(localPointsLocations, myCollection);
                //foreach (var central in orderedDistances)
                Parallel.ForEach(orderedDistances, central =>
                {
                    int hits = 0;
                    float distance = 0;
                    for (int q = 0; q < Query.Length; q++)
                    {
                        int positionX = central.info.X + PositionDifferenceX[s, q];
                        int positionY = central.info.Y + PositionDifferenceY[s, q];
                        var forWords = new List<Descriptor>();


                        // X + 1 - 1, Y
                        if (PositionIndex[central.info.Document].ContainsKey(positionY))
                        {
                            if (PositionIndex[central.info.Document][positionY].ContainsKey(positionX))
                                forWords.AddRange(PositionIndex[central.info.Document][positionY][positionX]);

                            if (PositionIndex[central.info.Document][positionY].ContainsKey(positionX - 1))
                                forWords.AddRange(PositionIndex[central.info.Document][positionY][positionX - 1]);

                            if (PositionIndex[central.info.Document][positionY].ContainsKey(positionX + 1))
                                forWords.AddRange(PositionIndex[central.info.Document][positionY][positionX + 1]);
                        }

                        // X + 1 - 1, Y - 1
                        if (PositionIndex[central.info.Document].ContainsKey(positionY - 1))
                        {
                            if (PositionIndex[central.info.Document][positionY - 1].ContainsKey(positionX))
                                forWords.AddRange(PositionIndex[central.info.Document][positionY - 1][positionX]);

                            if (PositionIndex[central.info.Document][positionY - 1].ContainsKey(positionX - 1))
                                forWords.AddRange(PositionIndex[central.info.Document][positionY - 1][positionX - 1]);

                            if (PositionIndex[central.info.Document][positionY - 1].ContainsKey(positionX + 1))
                                forWords.AddRange(PositionIndex[central.info.Document][positionY - 1][positionX + 1]);
                        }

                        // X + 1 - 1, Y + 1
                        if (PositionIndex[central.info.Document].ContainsKey(positionY + 1))
                        {
                            if (PositionIndex[central.info.Document][positionY + 1].ContainsKey(positionX))
                                forWords.AddRange(PositionIndex[central.info.Document][positionY + 1][positionX]);

                            if (PositionIndex[central.info.Document][positionY + 1].ContainsKey(positionX - 1))
                                forWords.AddRange(PositionIndex[central.info.Document][positionY + 1][positionX - 1]);

                            if (PositionIndex[central.info.Document][positionY + 1].ContainsKey(positionX + 1))
                                forWords.AddRange(PositionIndex[central.info.Document][positionY + 1][positionX + 1]);
                        }

                        if (forWords.Count() == 0) continue;
                        float localm = float.MaxValue;
                        foreach (var hwords in forWords)
                        {
                            var m = GetEuclideanCode(hwords.Value, Query[q].Descriptor, myCollection.ElementsClusters);
                            if (m < localm)
                            {
                                localm = m;
                            }
                        }
                        distance += localm;
                        hits++;
                    }
                    if (hits > 0)
                    {
                        double localDistance = distance / hits;
                        FinalResults.Add(GetWordRectangle(central.info.X, central.info.Y, QueryImage.Width, QueryImage.Height, distance / hits, central.info.Document, myCollection.Options.QX, myCollection.Options.QY));
                    }

                });

            }
            var FinalResultsList = FinalResults.PartialSortBy(2000, x => x.Distance).ToList();





            for (int i = 0; i < FinalResultsList.Count(); i++)
            {
                var cp = FinalResultsList[i];
                if (cp == null) continue;
                for (int j = i + 1; j < FinalResultsList.Count(); j++)
                {

                    var tempP = FinalResultsList[j];
                    if (tempP == null) continue;
                    if (tempP.Document != cp.Document) continue;
                    Rectangle overlap = cp.Intersect(tempP);
                    Rectangle union = cp.Union(tempP);// Math.Min(tempP.Area, cp.Area);
                    if (overlap != null)
                    {
                        if (overlap.Area / (float)union.Area >= 0.5)
                        {
                            if (tempP.Distance < cp.Distance)
                            {
                                FinalResultsList[i] = FinalResultsList[j];
                            }
                            else if (tempP.Distance == cp.Distance)
                            {
                                if (cp.Area < tempP.Area)
                                {
                                    FinalResultsList[i] = FinalResultsList[j];
                                }


                            }
                            FinalResultsList[j] = null;
                        }
                    }
                }
            }
            FinalResultsList.RemoveAll(r => r == null);
            return FinalResultsList;
        }




        private static QueryInvertIndexInfo[] GetQueryPoints(NewImage Img, double[][][] CoarsekClusters, double[][] ResidualClusters, int MultipleWordAssignements, CollectionOptions Options)
        {
            var myDolf = new DoLF.DoLF();
            var mypoints = myDolf.GetDSLPoints(Img.Data, Img.Width, Img.Height, 3,
                WindowSize: Options.WindowSize > 0 ? Options.WindowSize : 68,
                    MinCCSize: Options.MinCCSize,
                    DynamicWindow: Options.WindowSize == -1);


            var centerX = mypoints.Average(p => p.X);
            var centerY = mypoints.Average(p => p.Y);

            var dx = mypoints.Select(p => Math.Abs(p.X - centerX)).Average();
            var dy = mypoints.Select(p => Math.Abs(p.Y - centerY)).Average();

            var orderedFromCenterPoints = mypoints.OrderBy(p => (p.X - centerX) * (p.X - centerX) + (p.Y - centerY) * ((p.Y - centerY)));


            var pointsInfo = new List<QueryInvertIndexInfo>();
            foreach (var p in orderedFromCenterPoints)
            {
                var coarsetLabelsPerM = new List<int>[CoarsekClusters.Length];
                for (int m = 0; m < CoarsekClusters.Length; m++)
                {
                    var maxHeap = new MaxHeap<int>(MultipleWordAssignements);
                    for (int label = 0; label < CoarsekClusters[m].Length; label++)
                        maxHeap.AddItem(GetEuclideanCode(CoarsekClusters[m][label], Quantization.Transform(p.Descriptor, m)), label);
                    coarsetLabelsPerM[m] = new List<int>();
                    coarsetLabelsPerM[m].AddRange(maxHeap.GetList());
                }

                var coarsetLabels = new List<int>();
                foreach (var m0 in coarsetLabelsPerM[0])
                    foreach (var m1 in coarsetLabelsPerM[1])
                        foreach (var m2 in coarsetLabelsPerM[2])
                            foreach (var m3 in coarsetLabelsPerM[3])
                                coarsetLabels.Add(Quantization.GetCoarsekIndex(m0, m1, m2, m3, Options.CoarsekNumberCluster));


                pointsInfo.Add(new QueryInvertIndexInfo()
                {
                    CoarsetLabels = coarsetLabels,
                    X = p.X / Options.QX,
                    Y = p.Y / Options.QY,
                    Descriptor = p.Descriptor
                });
            }

            return pointsInfo.ToArray();
        }

        static int GetEuclideanCode(int[] word, int[] query)
        {
            int distance = 0;
            for (int d = 0; d < word.Length; d++)
                distance += (word[d] - query[d]) * (word[d] - query[d]);
            return distance;
        }

        static double GetEuclideanCode(double[] word, float[] query)
        {
            double distance = 0;
            for (int d = 0; d < word.Length; d++)
                distance += (word[d] - query[d]) * (word[d] - query[d]);
            return distance;
        }

        static float GetEuclideanCode(in byte[] word, in float[] query, in double[][] ElementsClusters)
        {
            double distance = 0;
            for (int d = 0; d < word.Length; d++)
            {
                var w = ElementsClusters[Convert.ToInt32(word[d])][0];
                distance += (w - query[d]) * (w - query[d]);
            }
            return (float)distance;
        }

        private static long GetHashCode(in int X, in int Y, in int D) => X + (501 * Y) + (501 * 501 * D);


        private static IEnumerable<DsLPoints> ReOrderClosestsToMeanXY(DsLPoints[] myDsLPs)
        {
            float meanX = 0, meanY = 0;
            foreach (var lp in myDsLPs)
            {
                meanX += lp.X;
                meanY += lp.Y;
            }

            meanX = meanX / myDsLPs.Length;
            meanY = meanY / myDsLPs.Length;


            //find the closest point
            var distances = new List<(DsLPoints Point, float distance)>();
            foreach (var lp in myDsLPs)
                distances.Add((lp, Math.Abs(lp.X - meanX) + Math.Abs(lp.Y - meanY)));

            return distances.OrderBy(x => x.distance).Select(x => x.Point);
        }

        private static Rectangle GetWordRectangle(int X, int Y, int QueryWidth, int QueryHeight, float Distance, int Document, int QX, int QY)
        {
            var rect = new Rectangle
            {
                X = X * QX - QueryWidth / 2,
                Y = Y * QY - QueryHeight / 2,
                Width = QueryWidth,
                Height = QueryHeight,
                Distance = Distance,
                Document = Document
            };
            if (rect.X < 0) rect.X = 0;
            if (rect.Y < 0) rect.Y = 0;
            return rect;
        }

        private static Dictionary<int, Descriptor> GetDescriptorsFromDataset(IEnumerable<int> Ids, CollectionDatabase DataCollection)
        {
            if (Ids.Count() == 0) return new Dictionary<int, Descriptor>();
            var sql = new StringBuilder("SELECT Id, X, Y, Document, Value From Descriptor WHERE Id In (");
            foreach (var id in Ids)
                sql.Append($" {id}, ");
            sql.Remove(sql.Length - 2, 2);
            sql.Append(")");
            return DataCollection.DescriptorsDB.Query<Descriptor>(sql.ToString()).ToDictionary(k => k.Id, v => v);
        }

        private static Dictionary<int, Descriptor> GetDescriptorsFromDataset2(IEnumerable<int> Ids, CollectionDatabase DataCollection)
        {
            if (Ids.Count() == 0) return new Dictionary<int, Descriptor>();
            var list = new ConcurrentBag<Descriptor>();
            //foreach (var batchsid in Ids.Batch(50000))
            Parallel.ForEach(Ids.Batch(50000), batchIds =>
            {
                GetDescriptorsFromDatasetList(batchIds, DataCollection).ForEach(d => list.Add(d));
            });
            return list.ToDictionary(k => k.Id, v => v);
        }

        private static Descriptor[] GetDescriptorsFromDatasetUsingCBF(List<int> Ids, CollectionDatabase DataCollection)
        {
            if (Ids.Count() == 0) return new Descriptor[0];
            //var list = new List<Descriptor>();
            var list = new Descriptor[Ids.Count()];
            for (int i = 0; i < Ids.Count(); i++)
            {
                list[i] = DataCollection.CBF.GetDescriptor(Ids[i]);
            }
            return list;//.ToDictionary(k => k.Id, v => v);
        }

        private static List<Descriptor> GetDescriptorsFromDatasetList(IEnumerable<int> Ids, CollectionDatabase DataCollection)
        {
            if (Ids.Count() == 0) return new List<Descriptor>();
            var sql = new StringBuilder("SELECT Id, X, Y, Document, Value From Descriptor WHERE Id In (");
            foreach (var id in Ids)
                sql.Append($" {id}, ");
            sql.Remove(sql.Length - 2, 2);
            sql.Append(")");
            return DataCollection.DescriptorsDB.Query<Descriptor>(sql.ToString());
        }

        private static Dictionary<int, Dictionary<int, Dictionary<int, List<Descriptor>>>> GetDescriptorsFromDatasetHashCode(IEnumerable<int> Ids, CollectionDatabase DataCollection)
        {

            Debug.WriteLine($"Total ids for GetDescriptorsFromDatasetHashCode: {Ids.Count()}");
            var result = new Dictionary<int, Dictionary<int, Dictionary<int, List<Descriptor>>>>(); // d, y, x


            var sql = new StringBuilder("SELECT Id, X, Y, Document, Value From Descriptor WHERE Hashkey IN (");
            foreach (var hashcode in Ids)
                sql.Append($" {hashcode}, ");
            sql.Remove(sql.Length - 2, 2);
            sql.Append(")");
            foreach (var r in DataCollection.DescriptorsDB.Query<Descriptor>(sql.ToString()))
            {
                if (!result.ContainsKey(r.Document)) result.Add(r.Document, new Dictionary<int, Dictionary<int, List<Descriptor>>>());
                if (!result[r.Document].ContainsKey(r.Y)) result[r.Document].Add(r.Y, new Dictionary<int, List<Descriptor>>());
                if (!result[r.Document][r.Y].ContainsKey(r.X)) result[r.Document][r.Y].Add(r.X, new List<Descriptor>());
                result[r.Document][r.Y][r.X].Add(r);
            }

            return result;
        }

        private static ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentBag<Descriptor>>>>
         GetDescriptorsFromDatasetHashCode2(IEnumerable<long> Ids, CollectionDatabase DataCollection)
        {
            Debug.WriteLine($"Total ids for GetDescriptorsFromDatasetHashCode: {Ids.Count()}");
            foreach (var batchIds in Ids.Distinct().Batch(40000))
            {
                foreach (var r in GetDescriptorsFromDatasetHashCodeListUsingCBF(batchIds, DataCollection))
                {
                    if (!PositionIndex.ContainsKey(r.Document)) PositionIndex.TryAdd(r.Document, new ConcurrentDictionary<int, ConcurrentDictionary<int, ConcurrentBag<Descriptor>>>());
                    if (!PositionIndex[r.Document].ContainsKey(r.Y)) PositionIndex[r.Document].TryAdd(r.Y, new ConcurrentDictionary<int, ConcurrentBag<Descriptor>>());
                    if (!PositionIndex[r.Document][r.Y].ContainsKey(r.X)) PositionIndex[r.Document][r.Y].TryAdd(r.X, new ConcurrentBag<Descriptor>());
                    PositionIndex[r.Document][r.Y][r.X].Add(r);
                }

            }
            return PositionIndex;
        }

        private static List<Descriptor> GetDescriptorsFromDatasetHashCodeList(IEnumerable<long> Ids, CollectionDatabase DataCollection)
        {
            var sql = new StringBuilder("SELECT Id, X, Y, Document, Value From Descriptor WHERE Hashkey IN (");
            foreach (var hashcode in Ids)
                sql.Append($" {hashcode}, ");
            sql.Remove(sql.Length - 2, 2);
            sql.Append(")");
            return DataCollection.DescriptorsDB.Query<Descriptor>(sql.ToString());
        }

        private static List<Descriptor> GetDescriptorsFromDatasetHashCodeListUsingCBF(IEnumerable<long> Ids, CollectionDatabase DataCollection)
        {
            var sql = new StringBuilder("SELECT Id From HashDescriptor WHERE Hashkey IN (");
            foreach (var hashcode in Ids)
                sql.Append($" {hashcode}, ");
            sql.Remove(sql.Length - 2, 2);
            sql.Append(")");
            var descriptors = new List<Descriptor>();
            List<HashDescriptor> hashes = DataCollection.DescriptorsDB.Query<HashDescriptor>(sql.ToString());
            lock (DataCollection.CBF.LockStream)
                foreach (var id in hashes)
                {
                    descriptors.Add(
                        DataCollection.CBF.GetDescriptor(id.Id)
                        );
                }

            return descriptors;
        }


    }
}
