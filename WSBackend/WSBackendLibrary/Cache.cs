﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using WSBackendLibrary.Models;

namespace WSBackendLibrary
{
    public static class Cache
    {
        public static ConcurrentDictionary<int, CollectionDatabase> Active = new ConcurrentDictionary<int, CollectionDatabase>();
    }
}
