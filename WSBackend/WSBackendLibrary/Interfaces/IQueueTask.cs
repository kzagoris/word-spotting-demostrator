﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WSBackendLibrary.Interfaces
{
    public interface IQueueTaskService
    {
        int CreateTask<T>(Func<T> process);
        bool? IsTaskComplete<T>(int TaskId);
        Task<T> GetTask<T>(int TaskId);
        T GetResult<T>(int TaskId);
    }
}
