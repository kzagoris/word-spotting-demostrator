namespace benchmark
{
    public class Descriptor
    {
        public int Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public long Hashkey { get; set; }
        public int Document { get; set; } // document index
        public byte[] Value { get; set; }
    }
}
