﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace benchmark
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<MeasureCustomBinaryFormatImplementations>();
            // var m = new MeasureCustomBinaryFormatImplementations();
            // m.RunCBFStackAlloc();
        }

        public class MeasureCustomBinaryFormatImplementations
        {
            private CustomBinaryFormat _cbf;
            private CustomBinaryFormatStackAlloc _cbfStackAlloc;
            private CustomBinaryFormatMemory _cbfMemory;
            private CustomBinaryFormatParallel _cbfParallel;
            private int[] Positions;


            public MeasureCustomBinaryFormatImplementations()
            {
                _cbf = new CustomBinaryFormat("C:\\Users\\kzago\\Desktop\\word-spotting-demostrator\\benchmark\\LocalPoints1.data", FileMode.Open, FileAccess.Read);
                _cbfStackAlloc = new CustomBinaryFormatStackAlloc("C:\\Users\\kzago\\Desktop\\word-spotting-demostrator\\benchmark\\LocalPoints4.data", FileMode.Open, FileAccess.Read);
                _cbfMemory = new CustomBinaryFormatMemory("C:\\Users\\kzago\\Desktop\\word-spotting-demostrator\\benchmark\\LocalPoints2.data", FileMode.Open, FileAccess.Read);
                _cbfParallel = new CustomBinaryFormatParallel("C:\\Users\\kzago\\Desktop\\word-spotting-demostrator\\benchmark\\LocalPoints3.data", FileMode.Open, FileAccess.Read);
                Positions = new int[200000];
                var rnd = new Random();
                for (int i = 0; i < Positions.Length; i++)
                {
                    Positions[i] = rnd.Next(2000000);
                }

            }

            [Benchmark]
            public IEnumerable<Descriptor> RunCBF()
            {
                var descriptors = new List<Descriptor>();
                foreach (var p in Positions)
                {
                    descriptors.Add(_cbf.GetDescriptor(p));
                }
                return descriptors;

            }

            [Benchmark]
            public IEnumerable<Descriptor> RunCBFStackAlloc()
            {
                var descriptors = new List<Descriptor>();
                foreach (var p in Positions)
                {
                    descriptors.Add(_cbfStackAlloc.GetDescriptor(p));
                }
                return descriptors;

            }

            [Benchmark]
            public IEnumerable<Descriptor> RunCBFParallel()
            {
                var descriptors = new ConcurrentBag<Descriptor>();
                Parallel.ForEach(Positions, p =>
                {
                    descriptors.Add(_cbfParallel.GetDescriptor(p));

                });
                return descriptors;

            }

            [Benchmark]
            public IEnumerable<Descriptor> RunCBFMemory()
            {
                var descriptors = new List<Descriptor>();
                foreach (var p in Positions)
                {
                    descriptors.Add(_cbfMemory.GetDescriptor(p));
                }
                return descriptors;

            }


        }
    }
}
