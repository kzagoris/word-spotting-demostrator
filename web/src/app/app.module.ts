import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';

import { AppRoutingModule } from './app-routing.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule, MatCheckboxModule, MatDialogModule, MatExpansionModule, MatFormFieldModule, MatGridListModule, MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressBarModule, MatProgressSpinnerModule, MatSelectModule, MatSlideToggleModule,
  MatToolbarModule
} from '@angular/material';
import { CollectionsComponent } from 'app/components/collections/collections.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { OverlayModule } from '@angular/cdk/overlay';
import { CreateCollectionComponent } from './components/create-collection/create-collection.component';
import { HttpClientModule } from '@angular/common/http';
import { WsService } from './providers/ws.service';
import { ShowCollectionComponent } from './components/show-collection/show-collection.component';
import { StorageService } from './providers/storage.service';
import { ShowDocumentComponent } from './components/show-document/show-document.component';
import { CacheService } from './providers/cache.service';
import { ShowResultsComponent } from './components/show-results/show-results.component';
import { DialogConfirmationComponent } from './components/dialog-confirmation/dialog-confirmation.component';
import { ReverseTruncatePipe } from './pipes/rtruncate.pipe';
import {MatListModule} from '@angular/material';
import {Ng2FileDropModule} from 'app/ng2-file-drop';
import {OptionsService} from './providers/options.service';
import {BrowseResultComponent} from './components/browse-results/browse-result.component';

import { environment } from '../environments/environment';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CollectionsComponent,
    CreateCollectionComponent,
    ShowCollectionComponent,
    ShowDocumentComponent,
    ShowResultsComponent,
    BrowseResultComponent,
    DialogConfirmationComponent,
    ReverseTruncatePipe
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: false }),
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatCardModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatProgressBarModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatGridListModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    OverlayModule,
    Ng2FileDropModule
  ],
  entryComponents: [DialogConfirmationComponent],
  providers: [WsService, StorageService, CacheService, OptionsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
