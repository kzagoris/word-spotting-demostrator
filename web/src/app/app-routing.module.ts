import {CollectionsComponent} from './components/collections/collections.component';
import {HomeComponent} from './components/home/home.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateCollectionComponent} from './components/create-collection/create-collection.component';
import {ShowCollectionComponent} from './components/show-collection/show-collection.component';
import {ShowDocumentComponent} from './components/show-document/show-document.component';
import {DocumentResolve} from './components/show-document/document-resolve';
import {SearchResolve} from './components/show-results/search-resolve';
import {ShowResultsComponent} from './components/show-results/show-results.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'collections',
    component: CollectionsComponent,
    data: {title: 'home', link: '/'}
  },
  {
    path: 'create-collection',
    component: CreateCollectionComponent
  },
  {
    path: 'collection/:collectionId',
    component: ShowCollectionComponent,
    data: {title: 'go back', link: '/collections'},
  },
  {
    path: 'search/:collectionId/:documentId/:cacheId',
    component: ShowResultsComponent,
    resolve: {document: SearchResolve}
  },
  {
    path: 'document/:collectionId/:documentId',
    component: ShowDocumentComponent,
    resolve: {document: DocumentResolve}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule],
  providers: [DocumentResolve, SearchResolve]
})
export class AppRoutingModule {
}
