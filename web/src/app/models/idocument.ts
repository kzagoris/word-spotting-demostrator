export interface IDocument {
  id: number,
  path: string
}
