export interface ISearchOptions {
  TakeLocalPointsSameQuantizationBucket: number;
  MultipleWordAssignments: number;
  NumberCenterLocalPoints: number;
}
