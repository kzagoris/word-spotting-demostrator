import {IDocument} from './idocument';
import {ICollectionOptions} from './icollection-options';

export interface ICollection {
  id?: number,
  name: string,
  documents: Array<IDocument>,
  options: ICollectionOptions
}
