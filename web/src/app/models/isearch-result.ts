import {Observable} from 'rxjs/Observable';

export interface ISearchResult {
  imageId: number;
  location: Array<number>; // x, y, width, height
  content?: string;
}

