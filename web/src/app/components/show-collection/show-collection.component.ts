import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {StorageService} from '../../providers/storage.service';
import {WsService} from '../../providers/ws.service';
import {ICollection} from '../../models/icollection';
import {IDocument} from '../../models/idocument';
import {CacheService} from '../../providers/cache.service';

@Component({
  selector: 'app-show-collection',
  templateUrl: './show-collection.component.html',
  styleUrls: ['./show-collection.component.scss']
})
export class ShowCollectionComponent implements OnInit {

  public collection: Promise<ICollection> | null = null;
  public documents: IDocument[];
  public documentsLength: number;
  public collectionId: string;



  constructor(route: ActivatedRoute, public router: Router, public storage: StorageService,
              public wsService: WsService) {
    route.params.subscribe(x => {
        this.collectionId = x.collectionId;
        wsService.activateCollection(x.collectionId).subscribe(a => {
          this.collection = storage.getDBCollection(x.collectionId);
          this.collection.then(d => {
            this.documentsLength = d.documents.length;
            this.display(0, 50);
          });
        });
      }
    );

  }

  public async display(index: number, pageSize: number) {
    const col = await this.collection;
    this.documents = col.documents.slice(index * pageSize, (index + 1) * pageSize);
  }

  public goToDocument(documentId: number) {
    this.router.navigate(['/document/', this.collectionId, documentId]);
  }

  getFilename(path: string): string {
    let filenames = path.split('/');

    if (filenames.length < 2) {
      filenames = path.split('\\');
    }
    return filenames.pop();
  }

  getThumbPath(path: string): string {

    let filenames = path.split('/');
    let separator = '/';

    if (filenames.length < 2) {
      filenames = path.split('\\');
      separator = '\\';
    }

    let thumbPath = '';
    for (let i = 0; i < filenames.length - 1; i++) {
      thumbPath += filenames[i] + separator;
    }
    thumbPath += 'thumbs' + separator + filenames.pop().slice(0, - 4) + '.jpg';
    return thumbPath;

  }


  ngOnInit() {
  }

}
