import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {IDocument} from '../../models/idocument';
import {StorageService} from '../../providers/storage.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge'
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/pairwise';
import 'rxjs/add/operator/takeLast';
import {WsService} from '../../providers/ws.service';
import {IQuery} from '../../models/iquery';
import 'rxjs/add/operator/publishReplay';
import {CacheService} from '../../providers/cache.service';
import {Subscription} from 'rxjs/Subscription';
import {ISearchOptions} from '../../models/isearch-options';
import {OptionsService} from '../../providers/options.service';

@Component({
  selector: 'app-show-document',
  templateUrl: './show-document.component.html',
  styleUrls: ['./show-document.component.scss']
})
export class ShowDocumentComponent implements OnDestroy {
  @ViewChild('mainDocument') mainDocumentRef: ElementRef;
  @ViewChild('query') query: ElementRef;
  @ViewChild('wordDiv') wordRef: ElementRef;
  public displayQuery: 'none' | 'block' = 'none';
  public document: IDocument;
  public queryImageBase64: string;
  public mousePressed = false;
  public word: { x: number, y: number, width: number, height: number } = {x: 0, y: 0, width: 0, height: 0};
  public displayWordDiv: 'none' | 'inherit' = 'none';
  private options$: Subscription;
  public options: ISearchOptions;
  private collectionId: number;
  private relativePath = '../WSBackend/WSBackendRESTApi/';

  constructor(private route: ActivatedRoute, public storage: StorageService, public router: Router,
              public wsService: WsService, public cache: CacheService, public optionsService: OptionsService) {
    route.params.subscribe(r => {
      this.collectionId = Number(r.collectionId);
      storage.getDBCollection(r.collectionId).then(s => {
        if (s.documents) {
          this.document = s.documents.find(c => c.id === Number(r.documentId));
        }
        if (!this.document) {
          router.navigate(['/collection', r.collectionId]);
        }
      });

    });

    this.options$ = this.optionsService.getSearchOptions().subscribe(v => this.options = v);

  }

  updateNumberCenterLocalPoints(value) {
    this.options.NumberCenterLocalPoints = parseInt(value);
    this.optionsService.updateSearchOptions(this.options);

  }

  updateMultipleWordAssignments(value) {
    this.options.MultipleWordAssignments = parseInt(value);
    this.optionsService.updateSearchOptions(this.options);
  }
  updateTakeLocalPointsSameQuantizationBucket(value) {
    this.options.TakeLocalPointsSameQuantizationBucket = parseInt(value);
    this.optionsService.updateSearchOptions(this.options);
  }

  search() {
    const query: IQuery = {
      collectionId: this.collectionId,
      queryImageBase64: this.queryImageBase64,
      options: this.options
    };
    const search$ = this.wsService.search(query);
    const cacheId = this.cache.setSearchResults(search$);
    this.router.navigate(['search', this.collectionId, this.document.id, cacheId]);
  }

  getFilename(path: string): string {
    let filenames = path.split('/');

    if (filenames.length < 2) {
      filenames = path.split('\\');
    }
    return filenames.pop();
  }

  ngOnDestroy(): void {
    this.options$.unsubscribe();
  }

  public onMainDocumentLoad() {

    const mainDocument: HTMLImageElement = this.mainDocumentRef.nativeElement;
    this.captureEvents(mainDocument);
  }

  private captureEvents(document: HTMLImageElement) {
    Observable
      .fromEvent(document, 'mousedown').subscribe((r: MouseEvent) => {
      if (!this.mousePressed) {
        this.displayWordDiv = 'inherit';
        this.mousePressed = true;
        const rect = document.getBoundingClientRect();
        this.word.x = r.clientX - rect.left;
        this.word.y = r.clientY - rect.top;
        this.word.height = 0;
        this.word.width = 0;
      }
    });

    document.addEventListener('mousemove', (r: MouseEvent) => {
      if (this.mousePressed) {
        const rect = document.getBoundingClientRect();
        this.updateWordDims(r, rect);
      }
    });
    this.wordRef.nativeElement.addEventListener('mousemove', (r: MouseEvent) => {
      if (this.mousePressed) {
        const rect = document.getBoundingClientRect();
        this.updateWordDims(r, rect);
      }
    });


    this.wordRef.nativeElement.addEventListener('mouseup', (r: MouseEvent) => {
      this.mouseUpEvent(r);
    });
    document.addEventListener('mouseup', (r: MouseEvent) => {
      this.mouseUpEvent(r);
    });


  }

  private updateWordDims(r: MouseEvent, rect: ClientRect) {
    const x = Math.round(r.clientX - rect.left);
    if (x < this.word.x) {
      this.word.width += Math.abs(this.word.x - x);
      this.word.x = x;
    } else if (x > this.word.x) {
      this.word.width = x - this.word.x;
    }

    const y = Math.round(r.clientY - rect.top);
    if (y < this.word.y) {
      this.word.height += this.word.y - y;
      this.word.y = y;
    } else if (y > this.word.y) {
      this.word.height = y - this.word.y;
    }
  }

  private mouseUpEvent(r: MouseEvent) {
    if (this.mousePressed) {
      const document = this.mainDocumentRef.nativeElement;
      this.mousePressed = false;
      const rect = document.getBoundingClientRect();
      this.updateWordDims(r, rect);
      const aspectRatio = document.naturalWidth / document.width;
      this.getRect(aspectRatio * this.word.x,
        aspectRatio * this.word.y,
        aspectRatio * this.word.width,
        aspectRatio * this.word.height);
    }
  }

  private getRect(x: number, y: number, width: number, height: number) {
    this.displayQuery = 'block';
    const canvasQuery: CanvasRenderingContext2D = this.query.nativeElement.getContext('2d');
    this.query.nativeElement.width = width;
    this.query.nativeElement.height = height;

    setTimeout(() => {
      canvasQuery.drawImage(this.mainDocumentRef.nativeElement, x, y, width, height, 0, 0, width, height);
      this.queryImageBase64 = this.query.nativeElement.toDataURL().replace(/^data:image\/(png|jpg);base64,/, '');
    }, 100);
  }



}
