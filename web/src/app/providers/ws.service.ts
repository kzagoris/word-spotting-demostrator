import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ICollectionListing } from '../models/icollection-listing';
import 'rxjs/add/operator/publishLast';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/retry';
import { ICollection } from '../models/icollection';
import { IQuery } from '../models/iquery';
import { ISearchResult } from '../models/isearch-result';
import 'rxjs/add/operator/shareReplay';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/publish';

@Injectable()
export class WsService {

  private url = 'http://orpheus.ee.duth.gr:5000';
  private _collectionList$: Observable<ICollectionListing[]>;

  constructor(public http: HttpClient) {
  }

  getCollectionListing(forceReload: boolean = false): Observable<ICollectionListing[]> {
    if (forceReload || this._collectionList$ == null) {
      this._collectionList$ = this.http.get<ICollectionListing[]>(`${this.url}/api/collections`).shareReplay(1);
    }
    return this._collectionList$;
  }

  getActiveCollectionListing = (): Observable<ICollectionListing[]> =>
    this._collectionList$ = this.http.get<ICollectionListing[]>(`${this.url}/api/collections/activate/list/`)
    .retry(3).publishLast().refCount();

  activateCollection = (id: number): Observable<boolean> =>
    this.http.get<boolean>(`${this.url}/api/collections/activate/${id}`)
      .publishLast().refCount();

  deactivateCollection = (id: number): Observable<boolean> =>
    this.http.get<boolean>(`${this.url}/api/collections/deactivate/${id}`).publishLast().refCount();

  async createCollection(collection: ICollection): Promise<number> {
    return new Promise<number>((resolve, reject) => {

      this.http.post(`${this.url}/api/collections`, collection, ).subscribe(
        (queue: any) => {
          if (queue.statusCode === 202) {
            const checkUrl: string = queue.headers[0].value[0];
            const checkId = setInterval(() => {
              this.http.get(checkUrl).subscribe(
                (check: any) => {
                  if (check.statusCode === 303) {
                    const resultsUrl: string = check.headers[0].value[0];
                    clearTimeout(checkId);
                    this.http.get<number>(resultsUrl).subscribe(
                      results => resolve(results),
                      error => {
                        console.log('Error during getting results', error);
                        return reject(error);
                      });
                  }
                },
                (error: HttpErrorResponse) => {
                  console.log('Error during check', error);
                  reject(error);
                })
            }, 1000);

          }
        },
        (error: HttpErrorResponse) => {
          console.log('Error during creating dataset task: ', error);
          reject(error);
        });
    });

  };

  deleteCollection = (id: number): Observable<object> => this.http.delete(`${this.url}/api/collections/${id}`);

  search(query: IQuery): Observable<Array<ISearchResult>> {
    return this.http.post<Array<ISearchResult>>(`${this.url}/api/search`, query);
  }

}
