import {Injectable} from '@angular/core';
import {ICollection} from '../models/icollection';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class StorageService {

  private url = 'http://orpheus.ee.duth.gr:5000/api/storage';


  constructor(private http: HttpClient) {
  }

  createDBCollection = (collection: ICollection): Promise<object>  =>
    this.http.post(this.url, collection).toPromise();


  getDBCollection(id: string): Promise<ICollection> {
    return new Promise<ICollection>((resolve, reject) => {
      return this.http.get<ICollection>(`${this.url}/${id}`).subscribe( col => {
        resolve(col);
      });
    });
  }


  deleteDBCollection = (id: string): Promise<object> =>
    this.http.delete(`${this.url}/${id}`).toPromise();

}
