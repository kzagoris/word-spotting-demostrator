<?php
$x = intval($_GET['x'] ?? -1);
$y = intval($_GET['y'] ?? -1);
$width = intval($_GET['width'] ?? -1);
$height = intval($_GET['height'] ?? -1);
$filename = $_GET['name'] ?? "";

if ($x == -1 || $y == -1 || $width == -1 || $height == - 1 || $filename == "" ) return;
$realpath = realpath($filename);
$basedir = "D:\webapps\Site\datasets";
if (strpos($realpath, $basedir)) return;
$im = imagecreatefromjpeg(realpath($filename));
$size = min(imagesx($im), imagesy($im));
$im2 = imagecrop($im, ['x' => $x, 'y' => $y, 'width' => $width, 'height' => $height]);
header("Content-Type: image/jpeg");
imagejpeg($im2);